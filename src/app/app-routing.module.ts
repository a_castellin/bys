import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'loading',
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'register-new-user',
    loadChildren: () => import('./pages/register-new-user/register-new-user.module').then( m => m.RegisterNewUserPageModule)
  },
  {
    path: 'password-recovery',
    loadChildren: () => import('./pages/password-recovery/password-recovery.module').then( m => m.PasswordRecoveryPageModule)
  },
  {
    path: 'elenco-appuntamenti',
    loadChildren: () => import('./pages/elenco-appuntamenti/elenco-appuntamenti.module').then( m => m.ElencoAppuntamentiPageModule)
  },
  {
    path: 'selezione-giorno',
    loadChildren: () => import('./pages/selezione-giorno/selezione-giorno.module').then( m => m.SelezioneGiornoPageModule)
  },
  {
    path: 'selezione-ora',
    loadChildren: () => import('./pages/selezione-ora/selezione-ora.module').then( m => m.SelezioneOraPageModule)
  },
  {
    path: 'selezione-marchio',
    loadChildren: () => import('./pages/selezione-marchio/selezione-marchio.module').then( m => m.SelezioneMarchioPageModule)
  },
  {
    path: 'selezione-negozio',
    loadChildren: () => import('./pages/selezione-negozio/selezione-negozio.module').then( m => m.SelezioneNegozioPageModule)
  },
  {
    path: 'loading',
    loadChildren: () => import('./pages/loading/loading.module').then( m => m.LoadingPageModule)
  },
  {
    path: 'dettaglio-appuntamento',
    loadChildren: () => import('./pages/dettaglio-appuntamento/dettaglio-appuntamento.module').then( m => m.DettaglioAppuntamentoPageModule)
  },
  {
    path: 'validate-user',
    loadChildren: () => import('./pages/validate-user/validate-user.module').then( m => m.ValidateUserPageModule)
  },
  {
    path: 'info',
    loadChildren: () => import('./pages/info/info.module').then( m => m.InfoPageModule)
  },
  {
    path: 'account',
    loadChildren: () => import('./pages/account/account.module').then( m => m.AccountPageModule)
  },
  {
    path: 'elenco-scontrini',
    loadChildren: () => import('./pages/elenco-scontrini/elenco-scontrini.module').then( m => m.ElencoScontriniPageModule)
  },
  {
    path: 'elenco-articoli',
    loadChildren: () => import('./pages/elenco-articoli/elenco-articoli.module').then( m => m.ElencoArticoliPageModule)
  },
  {
    path: 'dettaglio-articolo',
    loadChildren: () => import('./pages/dettaglio-articolo/dettaglio-articolo.module').then( m => m.DettaglioArticoloPageModule)
  },
  {
    path: 'elenco-carte',
    loadChildren: () => import('./pages/elenco-carte/elenco-carte.module').then( m => m.ElencoCartePageModule)
  },
  {
    path: 'dettaglio-carta',
    loadChildren: () => import('./pages/dettaglio-carta/dettaglio-carta.module').then( m => m.DettaglioCartaPageModule)
  },
  {
    path: 'selezione-marchio-carta',
    loadChildren: () => import('./pages/selezione-marchio-carta/selezione-marchio-carta.module').then( m => m.SelezioneMarchioCartaPageModule)
  },
  {
    path: 'selezione-articolo',
    loadChildren: () => import('./pages/selezione-articolo/selezione-articolo.module').then( m => m.SelezioneArticoloPageModule)
  },
  {
    path: 'selezione-tipologia',
    loadChildren: () => import('./pages/selezione-tipologia/selezione-tipologia.module').then( m => m.SelezioneTipologiaPageModule)
  },
  {
    path: 'change-password',
    loadChildren: () => import('./pages/change-password/change-password.module').then( m => m.ChangePasswordPageModule)
  },
  {
    path: 'notifiche',
    loadChildren: () => import('./pages/notifiche/notifiche.module').then( m => m.NotifichePageModule)
  },
  {
    path: 'promo',
    loadChildren: () => import('./pages/promo/promo.module').then( m => m.PromoPageModule)
  },
  {
    path: 'catalog',
    loadChildren: () => import('./pages/catalog/catalog.module').then( m => m.CatalogPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
