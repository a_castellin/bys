import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ImageModPageRoutingModule } from './image-mod-routing.module';

import { ImageModPage } from './image-mod.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ImageModPageRoutingModule
  ],
  declarations: [ImageModPage]
})
export class ImageModPageModule {}
