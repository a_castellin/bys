import { Component, OnInit } from '@angular/core';
import { 
ModalController, 
NavParams 
} from '@ionic/angular';

import * as Constants from '../../constants'

@Component({
  selector: 'app-image-mod',
  templateUrl: './image-mod.page.html',
  styleUrls: ['./image-mod.page.scss'],
})
export class ImageModPage implements OnInit {

  constructor(
    private modalController: ModalController,
    private navParams: NavParams
  ) { }

  imgRelPath:string;
  imgAbsPath:string;

  ngOnInit() {
    this.imgRelPath = this.navParams.data['imgRelPath'];
    this.imgAbsPath = this.navParams.data['imgAbsPath'];
  }

  async closeImage() {
    const onClosedData: string = 'OK!';
    await this.modalController.dismiss(onClosedData);
  }

  getImageUrl()
  {
    if(this.imgRelPath != undefined && this.imgRelPath != '')
      return Constants.IMAGE_BASE_URL + this.imgRelPath;
    else if(this.imgAbsPath != undefined && this.imgAbsPath != '')
      return this.imgAbsPath;
    else 
      return '../../../assets/images/no-image.png';
  }

}
