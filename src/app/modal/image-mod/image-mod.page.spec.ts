import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ImageModPage } from './image-mod.page';

describe('ImageModPage', () => {
  let component: ImageModPage;
  let fixture: ComponentFixture<ImageModPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImageModPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ImageModPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
