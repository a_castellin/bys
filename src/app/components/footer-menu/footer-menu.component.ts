import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import * as Constants from '../../constants'
import { SvcRequestsService } from '../../services/svc-requests.service';
import { MyUtils } from '../../services/my-utils.service'

@Component({
  selector: 'app-footer-menu',
  templateUrl: './footer-menu.component.html',
  styleUrls: ['./footer-menu.component.scss'],
})
export class FooterMenuComponent implements OnInit {
  @Input('user-info') public userInfo:any;
  @Input('marchio-info') public marchioInfo:any;
  @Input('negozio-info') public negozioInfo:any;
  @Input('selected-item-index') public selectedItemIndex:number = 0;
  NumNotifiche:number = 0;
  constructor(private router:Router, private SvcService:SvcRequestsService, private myUtils:MyUtils) { }

  ngOnInit() {
    if(this.selectedItemIndex != 2){
      this.SvcService.getNumeroNotifiche(this.userInfo['IDUTENTE']).subscribe(data=>{
        if(data != undefined && !isNaN(Number(data)))
        {
          this.NumNotifiche = <number>data;
        }
      })
    }
  }

  goToAppuntamenti()
  {
    if(this.selectedItemIndex != 1)
    {
      this.router.navigate(['/','elenco-appuntamenti'],{
        replaceUrl:true, 
        queryParams:{
          data:JSON.stringify(
            {
              "user":this.userInfo
            })
          }
        });
    }
  }

  goToNotifiche()
  {
    if(this.selectedItemIndex != 2)
    {
      this.router.navigate(['/','notifiche'],{
        replaceUrl:true, 
        queryParams:{
          data:JSON.stringify(
            {
              "user":this.userInfo,
              "marchio":this.userInfo,
              "negozio":this.userInfo
            })
          }
        });
    }
  }

  goToPromo()
  {
    if(this.selectedItemIndex != 3)
    {
      this.router.navigate(['/','promo'],{
        replaceUrl:true, 
        queryParams:{
          data:JSON.stringify(
            {             
              "user":this.userInfo,
              "marchio":this.marchioInfo,
              "negozio":this.negozioInfo
            })
          }
        });
    }
  }

  goToCatalogo()
  {
    if(this.selectedItemIndex != 4)
    {
      this.router.navigate(['/','catalog'],{
        replaceUrl:true, 
        queryParams:{
          data:JSON.stringify(
            {             
              "user":this.userInfo,
              "marchio":this.marchioInfo,
              "negozio":this.negozioInfo
            })
          }
        });
    }
  }
  


}
