import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { NgModule } from "@angular/core"
import { SideMenuComponent } from "./side-menu/side-menu.component"
import { FooterMenuComponent } from "./footer-menu/footer-menu.component"

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule],
    declarations: [SideMenuComponent,FooterMenuComponent],
    exports:[SideMenuComponent,FooterMenuComponent]
})

export class ComponentsModule{}