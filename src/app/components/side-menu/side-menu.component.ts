import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import * as Constants from '../../constants'
import { MyUtils } from '../../services/my-utils.service'
import { SvcRequestsService } from '../../services/svc-requests.service';


@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.scss'],
})
export class SideMenuComponent implements OnInit {
  @Input('user-info') public userInfo:any;

  constructor(private router:Router, private SvcService:SvcRequestsService, private myUtils:MyUtils) { }

  ngOnInit() {}

  goToInfo(){
    this.router.navigate(['/','info'],{replaceUrl:true, queryParams:{data:JSON.stringify({"user":this.userInfo})}});
  }

  getHelpPage()
  {
    this.myUtils.goToUrl(Constants.IMAGE_BASE_URL + 'Documenti/Help.htm');
    // window.open(Constants.IMAGE_BASE_URL + 'Documenti/Help.htm','_system', 'location=yes');
  }

  getMailAssistenza()
  {
    return Constants.ASSISTENZA;
  }

  goToAcccountDetail()
  {
    this.router.navigate(['/','account'],{replaceUrl:true, queryParams:{data:JSON.stringify({"user":this.userInfo})}});
  }

}
