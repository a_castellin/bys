import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AlertController } from '@ionic/angular';
import { FCM } from '@ionic-native/fcm/ngx';
import { Storage } from '@ionic/storage';
import { UniqueDeviceID } from '@ionic-native/unique-device-id/ngx';
import { MyUtils } from './services/my-utils.service'
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import {DomSanitizer} from '@angular/platform-browser'

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private alertController:AlertController,
    private fcm:FCM,
    private storage:Storage,
    private uuid:UniqueDeviceID,
    private myUtils:MyUtils,
    private iab:InAppBrowser,
    private domSanitizer:DomSanitizer
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      if (this.platform.is('android')) {
        this.platform.backButton.subscribeWithPriority(0, () => {
            this.chiudiApp()
        });
      }

      if(this.fcm.hasPermission()){

        //console.dir(this.device);
        // this.fcm.subscribeToTopic('marketing');
        this.refreshFCMToken()

        this.fcm.onNotification().subscribe(data => {
          var title = 'Notifica';
          var message = 'Notifica Ricevuta';
          var linkTitle = '';
          var linkUrl = '';
          if(data != undefined && data.title != undefined && data.body != undefined)
          {
            title = data.title;
            message = data.bodyFull;
            linkUrl = data.url;
          }

          this.notificaPush(title,message,linkUrl);

          if(data.wasTapped){
          } else {
          };
        });

        this.fcm.onTokenRefresh().subscribe(token => {
          // console.log("Token");
           console.dir(token)
          this.storage.set("fcm-token",token)
        });
      }
      else
      {
        this.notificaPushFailed();
      }

      // this.fcm.unsubscribeFromTopic('marketing');
      this.uuid.get().then((data:string) => {
        //console.log(data);
        this.storage.set("uuid",data)
      })
    });
  }

  refreshFCMToken()
  {
    this.fcm.getToken().then(token => {
      // console.log("Token");
       console.dir(token)
      if(token != undefined && token != null && token!='')
        this.storage.set("fcm-token",token);
      else
        this.refreshFCMToken();
    });
  }

  async chiudiApp() {
    const alert = await this.alertController.create({
      header: 'Chiusura applicazione!',
      message: "Confermi di voler chiudere l'applicazione?",
      buttons: [{
        text: 'Sì',
        handler: () => {
          navigator['app'].exitApp();
        }        
      },
      {
        text: 'No'
      }],
      mode:"ios"
    });

    await alert.present();
  }

  async notificaPush(title:string, msg:string, url:string) {

    if(url != undefined && url!= '')
    {
      const alert = await this.alertController.create({
        cssClass: 'PushNotificationAlert',
        header: 'Attenzione!',
        subHeader: title,
        message: msg,
        buttons: [{
          text:"Dettagli",
          cssClass:"alert-link-button",
          handler:()=>{
            this.myUtils.goToUrl(url);
          }
        },
        {
          text:'OK',
          handler:()=>{}
        }],
        mode:"ios"
      });

      await alert.present();
    }
    else
    {
      const alert = await this.alertController.create({
        cssClass: 'PushNotificationAlert',
        header: 'Attenzione!',
        subHeader: title,
        message: msg,
        buttons: ['OK'],
        mode:"ios"
      });

      await alert.present();
    }
  }

  async notificaPushFailed() {
    const alert = await this.alertController.create({
      cssClass: 'PushNotificationAlert',
      header: 'Attenzione!',
      //subHeader: title,
      message: "Push notification disabled",
      buttons: ['OK'],
      mode:"ios"
    });

    await alert.present();
  }
}
