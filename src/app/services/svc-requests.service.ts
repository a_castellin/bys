import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Storage } from '@ionic/storage';
import * as Constants from '../constants'
import { Device } from '@ionic-native/device';
import { CypherService } from './cypher.service'

@Injectable({
  providedIn: 'root'
})
export class SvcRequestsService {
  url = Constants.API_ENDPOIT;
  //url = 'https://bpcs.capthasystems.com/Bys.Web/WS/ccBysEXTWS.svc/';
  //url = 'http://172.22.201.157/BYS-WEB-APPLICATION_CC/Bys.Web/WS/ccBysEXTWS.svc/';
  
  secretKey = "bys";
  headers = new HttpHeaders().set("Content-type","application/json");
  lingua = "it-IT";
  /**
   * Constructor of the Service with Dependency Injection
   * @param http The standard Angular HttpClient to make requests
   */
  constructor(
    private http: HttpClient, 
    private storage:Storage,
    private cypher:CypherService) {
    //this.headers.set("Content-type","application/json");
    //this.headers.set("Access-Control-Allow-Origin","http://localhost");
   }

  /**
   * 
   * @param {String} username 
   * @param {String} password 
   */
  login(nome: string, numero:string, password:string){
    let body = {
      "SECRETKEY":this.secretKey,
      "NOME":Constants.EncryptEnable ? this.cypher.Encrypt(nome):nome,
      "CELLULARE":Constants.EncryptEnable ? this.cypher.Encrypt(numero):numero,
      "PASSWORD":Constants.EncryptEnable ? this.cypher.Encrypt(password):password
    }

    return this.http.post(this.url + "LoginUtenteWS",body,{headers:this.headers})
  }

  /**
   * 
   * @param username 
   * @param phone 
   */
  passwordRecovery(username: string, phone: string){
    let body = {
      "SECRETKEY":this.secretKey,
      "LINGUA_CULTUREINFO":this.lingua,
      "NOME":Constants.EncryptEnable ? this.cypher.Encrypt(username):username,
      "CELLULARE":Constants.EncryptEnable ? this.cypher.Encrypt(phone):phone
    }

    return this.http.post(this.url + "RecuperaPasswordWS",body,{headers:this.headers})
  }

  /**
   * 
   * @param username 
   * @param phone 
   */
  passwordChange(idUser: Number, passwordOld: string, passwordNew: string){
    let body = {
      "SECRETKEY":this.secretKey,
      "LINGUA_CULTUREINFO":this.lingua,
      "IDUTENTE":idUser,
      "PASSWORD_OLD":Constants.EncryptEnable ? this.cypher.Encrypt(passwordOld):passwordOld,
      "PASSWORD_NEW":Constants.EncryptEnable ? this.cypher.Encrypt(passwordNew):passwordNew
    }

    return this.http.post(this.url + "CambioPasswordWS",body,{headers:this.headers})
  }
  
  registerNewUser(nome: string, cognome: string, data:string, numero: string, email:string, username: string, password: string){
    let body = {
      "SECRETKEY":this.secretKey,
      "LINGUA_CULTUREINFO":this.lingua,
      "IDUTENTE":0,
      "USERNAME":"",
      "PASSWORD":Constants.EncryptEnable ? this.cypher.Encrypt(password):password,
      "NOME":Constants.EncryptEnable ? this.cypher.Encrypt(nome):nome,
      "COGNOME":Constants.EncryptEnable ? this.cypher.Encrypt(cognome):cognome,
      "CELLULARE":Constants.EncryptEnable ? this.cypher.Encrypt(numero):numero,
      "EMAIL":Constants.EncryptEnable ? this.cypher.Encrypt(email):email,
      "DATA_NASCITA_STR":data
    }

    return this.http.post(this.url + "UpdateUtenteWS",body,{headers:this.headers})
  }

  updateUser(nome: string, cognome: string, data:string, numero: string, email:string, idUtente:string, password:string){
    let body = {
      "SECRETKEY":this.secretKey,
      "LINGUA_CULTUREINFO":this.lingua,
      "IDUTENTE":idUtente,
      "USERNAME":"",
      "PASSWORD":Constants.EncryptEnable ? this.cypher.Encrypt(password):password,
      "NOME":Constants.EncryptEnable ? this.cypher.Encrypt(nome):nome,
      "COGNOME":Constants.EncryptEnable ? this.cypher.Encrypt(cognome):cognome,
      "CELLULARE":Constants.EncryptEnable ? this.cypher.Encrypt(numero):numero,
      "EMAIL":Constants.EncryptEnable ? this.cypher.Encrypt(email):email,
      "DATA_NASCITA_STR":data
    }

    return this.http.post(this.url + "UpdateUtenteWS",body,{headers:this.headers})
  }

  //updateUser(idUser: string){}//DA DEFINIRE

  /**
   * 
   * @param idUser 
   */
  getMarchi(idUser: Number)
  {
    let body = {
      "SECRETKEY":this.secretKey,
      "LINGUA_CULTUREINFO":this.lingua,
      "IDUTENTE":idUser
    }

    return this.http.post(this.url + "GetAziendeWS",body,{headers:this.headers})

  }

  /**
   * 
   * @param idUser 
   * @param brand 
   */
  getNegozi(idUser: Number, idAzienda:Number, localita:String, CAP:String, Lat?:Number, Long?:Number)
  {
    let body = {
      "GRIDDATAREQUEST":{
        "LINGUA_CULTUREINFO":this.lingua,
        "SECRETKEY":this.secretKey,
        "IDUTENTE":idUser,
        "IDAZIENDA":idAzienda,
        "CAP":CAP,
        "LOCALITA":localita,
      }
    };

    if(Lat != undefined)
      body['GRIDDATAREQUEST']['LAT'] = Lat;
    if(Long != undefined)
      body['GRIDDATAREQUEST']['LONG'] = Long;

    return this.http.post(this.url + "GetNegoziWS",body,{headers:this.headers});
  }

  /**
   * 
   * @param idUser 
   * @param idMarchio 
   * @param idNegozio 
   * @param date 
   * @param isFull 
   */
  getAppuntamenti(idUser: string, idAzienda: string, idNegozio:string, date:string, isFull:boolean)
  {
    let body = {
      "GRIDDATAREQUEST":{
        "LINGUA_CULTUREINFO":this.lingua,
        "SECRETKEY":this.secretKey,
        "IDUTENTE":idUser,
        "IDAZIENDA":idAzienda,
        "IDNEGOZIO":idNegozio,
        "DATA_APPUNTAMENTO":date,
        "FLAG_ALL":isFull,
        "NUMERO_POSTI":1
      }
    };

    return this.http.post(this.url + "GetAppuntamentiWS",body,{headers:this.headers});

  }

  getGiorni(idUser: string, idAzienda: string, idNegozio:string)
  {
    let body = {
      "GRIDDATAREQUEST":{
        "LINGUA_CULTUREINFO":this.lingua,
        "SECRETKEY":this.secretKey,
        "IDUTENTE":idUser,
        "IDAZIENDA":idAzienda,
        "IDNEGOZIO":idNegozio,
        "DATA_APPUNTAMENTO":"",
        "FLAG_ALL":false,
        "NUMERO_POSTI":1
      },
      "NUM_GIORNI":7
    };

    return this.http.post(this.url + "GetElencoGiorniWS",body,{headers:this.headers});
  }

  deleteAppuntamento(idUser: string, idAzienda: string, idNegozio:string, strDataOra:string)
  {
    let body = {
      "LINGUA_CULTUREINFO":this.lingua,
      "SECRETKEY":this.secretKey,
      "IDUTENTE":idUser,
      "IDAZIENDA":idAzienda,
      "IDNEGOZIO":idNegozio,
      "DATA_APPUNTAMENTO": strDataOra,
      "TIPOAP_CODICE":1
    };

    return this.http.post(this.url + "DeleteAppuntamentoWS",body,{headers:this.headers});
  }

  updateAppuntamento(idUser: string, idAzienda: string, idNegozio:string, strDataOra:string, nota:string, tipoAppuntamento:Number)
  {
    let body = {
      "LINGUA_CULTUREINFO":this.lingua,
      "SECRETKEY":this.secretKey,
      "IDUTENTE":idUser,
      "IDAZIENDA":idAzienda,
      "IDNEGOZIO":idNegozio,
      "DATA_APPUNTAMENTO": strDataOra,
      "TIPOAP_CODICE":tipoAppuntamento,
      "NOTA":nota
    };

    return this.http.post(this.url + "UpdateAppuntamentoWS",body,{headers:this.headers});
  }

  getTipiAppuntamento(idUser: string, idAzienda: string, idNegozio:string)
  {
    let body = {
      "LINGUA_CULTUREINFO":this.lingua,
      "SECRETKEY":this.secretKey,
      "IDUTENTE":idUser,
      "IDAZIENDA":idAzienda,
      "IDNEGOZIO":idNegozio
    };

    return this.http.post(this.url + "GetTipiAppuntamentoWS",body,{headers:this.headers});
  }

  setFCMToken(idUser:string)
  {
    this.storage.get("fcm-token").then((token)=>{
      //console.log(token)
      if(token != undefined && token != '')
      {
        this.storage.get("uuid").then((uuid)=>{
          if(uuid != undefined && uuid != '')
          {
            let today = new Date();
            uuid = (Device.uuid != undefined && Device.uuid != null && Device.uuid != '') ? Device.uuid:('uuid-' + today.getFullYear()+today.getMonth());
          }

          let body = {
            "LINGUA_CULTUREINFO":this.lingua,
            "SECRETKEY":this.secretKey,
            "IDUTENTE":idUser,
            "IDDISPOSITIVO":uuid,
            "TOKEN":token
          };

          this.http.post(this.url + "UpdateTokenWS",body,{headers:this.headers}).subscribe((data)=>{
            //console.dir(data);
          })

        });
      }
    });
  }

  validateUser(idUser:string, validationCode:string)
  {
    let body = {
      "LINGUA_CULTUREINFO":this.lingua,
      "SECRETKEY":this.secretKey,
      "IDUTENTE":idUser,
      "CODICE_SMS":validationCode
    };

    return this.http.post(this.url + "CheckSMSWS",body,{headers:this.headers});
  }

  sendNewCode(idUser:string)
  {
    let body = {
      "LINGUA_CULTUREINFO":this.lingua,
      "SECRETKEY":this.secretKey,
      "IDUTENTE":idUser
    };

    return this.http.post(this.url + "SendNewSMSWS",body,{headers:this.headers});
  }

  /**
   * 
   * @param idUser 
   * @param idMarchio 
   * @param idNegozio 
   * @param date 
   * @param isFull 
   */
  getArticoliSelezionati(idUser: string, idAzienda: string, idNegozio:string, date:string)
  {
    let body = {
      "LINGUA_CULTUREINFO":this.lingua,
      "SECRETKEY":this.secretKey,
      "IDUTENTE":idUser,
      "IDAZIENDA":idAzienda,
      "IDNEGOZIO":idNegozio,
      "DATA_APPUNTAMENTO":date
    };

    return this.http.post(this.url + "GetCapiSelezionatiWS",body,{headers:this.headers});

  }

  getScontrini(idUser: string, idAzienda: string, idNegozio:string, date:string)
  {
    let body = {
      "LINGUA_CULTUREINFO":this.lingua,
      "SECRETKEY":this.secretKey,
      "IDUTENTE":idUser,
      "IDAZIENDA":idAzienda,
      "IDNEGOZIO":idNegozio,
      "FIDELITY_CODICE":"",
      "NUMERO_GIORNI":0,
      "DATA_APPUNTAMENTO":date
    }
    return this.http.post(this.url + "GetScontriniUtenteEXTWS",body,{headers:this.headers});
  }

  getDettaglioScontrino(idUser: string, idAzienda: string, idNegozio:string, barcode:string, date:string)
  {
    let body = {
      "LINGUA_CULTUREINFO":this.lingua,
      "SECRETKEY":this.secretKey,
      "IDUTENTE":idUser,
      "IDAZIENDA":idAzienda,
      "IDNEGOZIO":idNegozio,
      "BARCODE_SCONTRINO":barcode,
      "DATA_APPUNTAMENTO":date
    }
    return this.http.post(this.url + "GetDettaglioScontrinoEXTWS",body,{headers:this.headers});
  }

  getTaglieValide(idUser: string, idAzienda: string, idNegozio:string, articoloCodice:string, coloreCodice:string, tagliaPosizione:string)
  {
    let body = {
      "LINGUA_CULTUREINFO":this.lingua,
      "SECRETKEY":this.secretKey,
      "IDUTENTE":idUser,
      "IDAZIENDA":idAzienda,
      "IDNEGOZIO":idNegozio,
      "ARTICOLO_CODICE":articoloCodice,
      "COLORE_CODICE":coloreCodice,
      "TAGLIA_POSIZIONE":tagliaPosizione
    }
    return this.http.post(this.url + "GetTaglieDisponibiliNegozioEXTWS",body,{headers:this.headers});
  }

  saveCambioReso(user:any, azienda:any, negozio:any, appuntamento:any, articolo:any, barcode:string, qta:number, arTaglieValide:any[])
  {
    let dtData = new Date(appuntamento['DATA_STR']);
    let strDataOra = dtData.getFullYear()+""+(dtData.getMonth() + 1).toString().padStart(2,"0")+""+dtData.getDate().toString().padStart(2,"0") + "/" + appuntamento['ORA_STR'];
    
    let arTaglie:any[] = [];

    for(let i=0; i<arTaglieValide.length; i++)
    {
      if(arTaglieValide[i]['QTA'] > 0)
      {
        let item = {
          "TAGLIA_POS_NEW":arTaglieValide[i]['TAGLIA_POSIZIONE'],
          "TAGLIA_DESC_NEW":arTaglieValide[i]['TAGLIA_DESCRIZIONE'],
          "QTA_NEW":arTaglieValide[i]['QTA'],
          "BARCODE_NEW":arTaglieValide[i]['ARTICOLO_BARCODE']
        }
        arTaglie.push(item);
      }
    }

    let body = {
      "LINGUA_CULTUREINFO":this.lingua,
      "SECRETKEY":this.secretKey,
      "IDUTENTE":user['IDUTENTE'],
      "IDAZIENDA":azienda['CODICE'],
      "IDNEGOZIO":negozio['IDNEGOZIO'],
      "DATA_APPUNTAMENTO_STR":strDataOra,
      "BARCODE_SCONTRINO":barcode,
      "ARTICOLO_CODICE":articolo['ARTICOLO_CODICE'],
      "ARTICOLO_DESC":articolo['ARTICOLO_DESCRIZIONE'],
      "ARTICOLO_IMAGE":articolo['ARTICOLO_IMAGE'], //AC_20200604
      "COLORE_CODICE":articolo['COLORE_CODICE'],
      "COLORE_DESC":articolo['COLORE_DESCRIZIONE'],
      "TAGLIA_POS_OLD":articolo['TAGLIA_POSIZIONE'],
      "TAGLIA_DESC_OLD":articolo['TAGLIA_DESCRIZIONE'],
      "BARCODE_OLD":articolo['ARTICOLO_BARCODE'],
      "QTA_RESO":qta,
      "LST_TG":arTaglie
    }
    return this.http.post(this.url + "SaveCambioResoWS",body,{headers:this.headers});
  }

  removeCambioReso(userId:string, aziendaId:string, negozioId:string, strDataOra:string, articolo:any)
  {
    let body = {
      "LINGUA_CULTUREINFO":this.lingua,
      "SECRETKEY":this.secretKey,
      "IDUTENTE":userId,
      "IDAZIENDA":aziendaId,
      "IDNEGOZIO":negozioId,
      "DATA_APPUNTAMENTO_STR":strDataOra,
      "BARCODE_SCONTRINO":articolo['BARCODE_SCONTRINO'],
      "ARTICOLO_CODICE":articolo['ARTICOLO_CODICE'],
      "COLORE_CODICE":articolo['COLORE_CODICE'],
      "TAGLIA_POS_OLD":articolo['TAGLIA_POS_OLD'],
    }

    return this.http.post(this.url + "DeleteCambioResoWS",body,{headers:this.headers});
  }

  getNotifiche(idUser: string)
  {
    let body = {
      "LINGUA_CULTUREINFO":this.lingua,
      "SECRETKEY":this.secretKey,
      "IDUTENTE":idUser
    };

    return this.http.post(this.url + "GetNotificheWS",body,{headers:this.headers});
  }

  updateNotifiche(notifica: any)
  {
    let body = {
      "LINGUA_CULTUREINFO":this.lingua,
      "SECRETKEY":this.secretKey,
      "IDUTENTE":notifica['IDUTENTE'],
      "IDNOTIFICA":notifica['IDNOTIFICA']
    };

    return this.http.post(this.url + "UpdateNotificaWS",body,{headers:this.headers});
  }

  getNumeroNotifiche(idUser: string)
  {
    let body = {
      "LINGUA_CULTUREINFO":this.lingua,
      "SECRETKEY":this.secretKey,
      "IDUTENTE":idUser
    };

    return this.http.post(this.url + "GetNotificheDaLeggereWS",body,{headers:this.headers});
  }

  getPromo(userId:string, aziendaId:string, negozioId:string)
  {
    let body = {
      "LINGUA_CULTUREINFO":this.lingua,
      "SECRETKEY":this.secretKey,
      "IDUTENTE":userId,
      "IDAZIENDA":aziendaId,
      "IDNEGOZIO":negozioId
    }

    return this.http.post(this.url + "GetElencoPromoEXTWS",body,{headers:this.headers});
  }

  getCataloghi(userId:string)
  {
    let body = {
      "LINGUA_CULTUREINFO":this.lingua,
      "SECRETKEY":this.secretKey,
      "IDUTENTE":userId
    }

    return this.http.post(this.url + "GetElencoCataloghiEXTWS",body,{headers:this.headers});
  }


}