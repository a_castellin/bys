import { TestBed } from '@angular/core/testing';

import { MyUtils } from './my-utils.service';

describe('MyUtils', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MyUtils = TestBed.get(MyUtils);
    expect(service).toBeTruthy();
  });
});
