import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Storage } from '@ionic/storage';
import * as Constants from '../constants'
import { Device } from '@ionic-native/device';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

@Injectable({
  providedIn: 'root'
})
export class MyUtils {

  constructor(private iab: InAppBrowser) { }

  goToUrl(sUrl:string)
  {
    this.iab.create(sUrl,'_system','location=yes');
  }
}
