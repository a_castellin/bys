import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Storage } from '@ionic/storage';
import * as Constants from '../constants'
import { Device } from '@ionic-native/device';
import * as CryptoJS from 'crypto-js';

@Injectable({
  providedIn: 'root'
})
export class CypherService {

  constructor() { }

  Encrypt(text:string){
    let key = CryptoJS.enc.Utf8.parse(Constants.AES_KEY);
    let IV = CryptoJS.enc.Utf8.parse(Constants.AES_IV);

    var encrypted = CryptoJS.AES.encrypt(text, key, {
      keySize: 256 / 8,
      iv: IV,
      mode: CryptoJS.mode.CBC,
      padding: CryptoJS.pad.Pkcs7
    });
    return CryptoJS.enc.Hex.stringify(encrypted.ciphertext);
  }
 
  Decrypt(text:string){
    let key = CryptoJS.enc.Utf8.parse(Constants.AES_KEY);
    let IV = CryptoJS.enc.Utf8.parse(Constants.AES_IV);

    let _text = CryptoJS.enc.Hex.parse(text);
    let _base64Text = _text.toString(CryptoJS.enc.Base64)

    var decrypted = CryptoJS.AES.decrypt(_base64Text, key, {
      keySize: 256 / 8,
      iv: IV,
      mode: CryptoJS.mode.CBC,
      padding: CryptoJS.pad.Pkcs7
    });

    var decrypted = CryptoJS.AES.decrypt(text, key, {
      keySize: 256 / 8,
      iv: IV,
      mode: CryptoJS.mode.CBC,
      padding: CryptoJS.pad.Pkcs7,
      format: CryptoJS.format.Hex
    });

    return decrypted.toString(CryptoJS.enc.Utf8);
  }


}
