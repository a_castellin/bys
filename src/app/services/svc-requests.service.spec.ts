import { TestBed } from '@angular/core/testing';

import { SvcRequestsService } from './svc-requests.service';

describe('SvcRequestsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SvcRequestsService = TestBed.get(SvcRequestsService);
    expect(service).toBeTruthy();
  });
});
