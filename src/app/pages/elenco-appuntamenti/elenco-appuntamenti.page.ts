import { Component, OnInit} from '@angular/core';
import { SvcRequestsService } from '../../services/svc-requests.service';
import { from } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import * as Constants from '../../constants'
import { MenuController } from '@ionic/angular';
import { MyUtils } from '../../services/my-utils.service'

// Typescript custom enum for search types (optional)
export enum DataFormatType {
  DDDMMMYYYY = 0,
  YYYYMMDD = 1
}

@Component({
  selector: 'app-elenco-appuntamenti',
  templateUrl: './elenco-appuntamenti.page.html',
  styleUrls: ['./elenco-appuntamenti.page.scss'],
})
export class ElencoAppuntamentiPage implements OnInit{
  arAppuntamenti: any[]=[];
  userInfo: any;
  //marchioInfo: any;
  //negozioInfo: any;
  city: string;
  constructor(
    private router: Router, 
    private activatedRoute: ActivatedRoute, 
    private SvcService: SvcRequestsService, 
    private alertController: AlertController,
    private menu:MenuController,
    private myUtils:MyUtils) { }

  Months: string[]=["Gen","Feb","Mar","Apr","Mag","Giu","Lug","Ago","Set","Ott","Nov","Dec"];
  Days: string[]=["Dom","Lun","Mar","Mer","Gio","Ven","Sab"];
  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(
      params => {
        let requestData = JSON.parse(params['data']);
        this.userInfo = requestData.user;
        this.updateAppuntamentiList();

      }
    );
  }

  getDataString(date:string)
  {
    let CurrentDate = new Date(date);
    return this.Days[CurrentDate.getDay()] + " " + CurrentDate.getDate() + " " + this.Months[CurrentDate.getMonth()] + " " + CurrentDate.getFullYear();
  }

  getElencoDate()
  {
    let arDate = [];
    let map = new Map()
    for(let item of this.arAppuntamenti)
    {
      if(!map.has(item['DATA_STR']))
      {
        map.set(item['DATA_STR'],true);
        arDate.push(item['DATA_STR']);
      }
    }
    return arDate.sort((a,b)=>{
      if(b['DATA_STR'] > a['DATA_STR'])
        return 1;
      else if(b['DATA_STR'] < a['DATA_STR'])
        return -1;
      else
        return 0
    });;
  }

  getAppuntamenti(date:string)
  {
    return this.arAppuntamenti.filter(item=>{return item['DATA_STR'] == date}).sort((a,b)=>{
      if(a['ORA_STR'] > b['ORA_STR'])
        return 1;
      if(a['ORA_STR'] < b['ORA_STR'])
        return -1;
      return 0;
      
    });
  }

  nuovoAppuntamento()
  {
    this.router.navigate(["/","selezione-marchio"],
    {
      replaceUrl:true, 
      queryParams:{
        data:JSON.stringify(
        {
          "user":this.userInfo,
          "autoSelect":true
          //"marchio":this.marchioInfo,
          //"negozio":this.negozioInfo
        })
      }
    });
  }

  updateAppuntamentiList(){
    this.SvcService.getAppuntamenti(this.userInfo['IDUTENTE'],"0","0","",false).subscribe(data=>{
      var success = (data["SUCCESS"]!=undefined?new Boolean(data["SUCCESS"]):false);
        if(success==true)
        {
          // console.dir(data["DATA"]);
          let Info = data["DATA"][0];
          this.arAppuntamenti = Info['ROWS'];
          //DA VALUTARE SE NECESSARIO RIFORMATTARE DATA YYYYMMDD/HH:mm
          for(let i=0 ; i<this.arAppuntamenti.length; i++)
          {
            let item = this.arAppuntamenti[i];
            let strData:string = item['DATA_APPUNTAMENTO_STR'];
            item['DATA_STR'] = strData.substr(0,4) + '-' + strData.substr(4,2) + '-' + strData.substr(6,2);
            item['ORA_STR'] = strData.substr(9,2) + ':' + strData.substr(12,2);
          }

          this.arAppuntamenti = this.arAppuntamenti.sort((a,b)=>{
            if(a['DATA_APPUNTAMENTO_STR'] < b['DATA_APPUNTAMENTO_STR'])
              return -1;
            if(a['DATA_APPUNTAMENTO_STR'] > b['DATA_APPUNTAMENTO_STR'])
              return 1;
            return 0;
          });

        }
    })
  }

  editAppuntamento(item)
  {
    this.router.navigate(["/","dettaglio-appuntamento"],
    {
      replaceUrl:true, 
      queryParams:{
        data:JSON.stringify(
        {
          "user":this.userInfo,
          "marchio":item['AZIENDA_INFO'],
          "negozio":item['NEGOZIO_INFO'],
          "appuntamento":item,
          "fromPage":"elenco-appuntamenti"
        })
      }
    });
  }

  logOut()
  {
    this.router.navigate(["/","login"],{replaceUrl:true});
  }

  getIcon(item)
  {
    if(item['AZIENDA_INFO']!=undefined)
    {
      if(item['AZIENDA_INFO']['ICON']!=undefined && item['AZIENDA_INFO']['ICON']!= '')
        // return Constants.IMAGE_BASE_URL + item['AZIENDA_INFO']['ICON'];
        return item['AZIENDA_INFO']['ICON'];
      
      if(item['AZIENDA_INFO']['IMG']!=undefined && item['AZIENDA_INFO']['IMG']!= '')
        // return Constants.IMAGE_BASE_URL + item['AZIENDA_INFO']['IMG'];
        return item['AZIENDA_INFO']['IMG'];
    }

    return ''
  }

  checkIcon(item)
  {
    if(item['AZIENDA_INFO']!=undefined)
    {
      if(item['AZIENDA_INFO']['ICON']!=undefined && item['AZIENDA_INFO']['ICON']!= '')
        return true;
      
      if(item['AZIENDA_INFO']['IMG']!=undefined && item['AZIENDA_INFO']['IMG']!= '')
        return true;
    }

    return false;
  }
  
  openSideMenu(){
    this.menu.open('side')
  }

  // goToNotifiche(){
  //   this.router.navigate(["/","notifiche"],
  //   {
  //     replaceUrl:true, 
  //     queryParams:{
  //       data:JSON.stringify(
  //       {
  //         "user":this.userInfo
  //       })
  //     }
  //   });
  // }

  // goToPromo(){
    
  // }

}
