import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ElencoAppuntamentiPage } from './elenco-appuntamenti.page';

describe('ElencoAppuntamentiPage', () => {
  let component: ElencoAppuntamentiPage;
  let fixture: ComponentFixture<ElencoAppuntamentiPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElencoAppuntamentiPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ElencoAppuntamentiPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
