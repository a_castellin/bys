import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ElencoAppuntamentiPage } from './elenco-appuntamenti.page';

const routes: Routes = [
  {
    path: '',
    component: ElencoAppuntamentiPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ElencoAppuntamentiPageRoutingModule {}

