import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ElencoAppuntamentiPageRoutingModule } from './elenco-appuntamenti-routing.module';

import { ElencoAppuntamentiPage } from './elenco-appuntamenti.page';
import { ComponentsModule } from '../../components/components.module'

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ElencoAppuntamentiPageRoutingModule,
    ComponentsModule
  ],
  declarations: [ElencoAppuntamentiPage]
})
export class ElencoAppuntamentiPageModule {}
