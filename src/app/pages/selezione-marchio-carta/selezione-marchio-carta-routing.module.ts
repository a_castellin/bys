import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SelezioneMarchioCartaPage } from './selezione-marchio-carta.page';

const routes: Routes = [
  {
    path: '',
    component: SelezioneMarchioCartaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SelezioneMarchioCartaPageRoutingModule {}
