import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SelezioneMarchioCartaPage } from './selezione-marchio-carta.page';

describe('SelezioneMarchioCartaPage', () => {
  let component: SelezioneMarchioCartaPage;
  let fixture: ComponentFixture<SelezioneMarchioCartaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelezioneMarchioCartaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SelezioneMarchioCartaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
