import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SelezioneMarchioCartaPageRoutingModule } from './selezione-marchio-carta-routing.module';

import { SelezioneMarchioCartaPage } from './selezione-marchio-carta.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SelezioneMarchioCartaPageRoutingModule
  ],
  declarations: [SelezioneMarchioCartaPage]
})
export class SelezioneMarchioCartaPageModule {}
