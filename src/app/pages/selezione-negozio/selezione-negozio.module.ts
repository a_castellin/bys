import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SelezioneNegozioPageRoutingModule } from './selezione-negozio-routing.module';

import { SelezioneNegozioPage } from './selezione-negozio.page';
import { SideMenuComponent } from '../../components/side-menu/side-menu.component'
import { ComponentsModule } from '../../components/components.module'

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SelezioneNegozioPageRoutingModule,
    ComponentsModule
  ],
  declarations: [SelezioneNegozioPage]
})
export class SelezioneNegozioPageModule {}
