import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SelezioneNegozioPage } from './selezione-negozio.page';

const routes: Routes = [
  {
    path: '',
    component: SelezioneNegozioPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SelezioneNegozioPageRoutingModule {}
