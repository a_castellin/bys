import { Component, OnInit, AfterViewInit } from '@angular/core';
import { SvcRequestsService } from '../../services/svc-requests.service';
import { from } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import * as Constants from '../../constants'
import { MenuController } from '@ionic/angular';
import { MyUtils } from '../../services/my-utils.service'


@Component({
  selector: 'app-selezione-negozio',
  templateUrl: './selezione-negozio.page.html',
  styleUrls: ['./selezione-negozio.page.scss'],
})
export class SelezioneNegozioPage implements OnInit, AfterViewInit{
  arNegozi: any[]=[];
  userInfo: any;
  marchioInfo: any;
  city: string = 'Biella';
  autoSelect: boolean = false;
  constructor(private router: Router, private activatedRoute: ActivatedRoute, private SvcService: SvcRequestsService,private menu:MenuController,
    private myUtils:MyUtils) { }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(
      params => {
         let requestData = JSON.parse(params['data']);
         this.userInfo = requestData.user;
         this.marchioInfo = requestData.marchio
         let auto = requestData.autoSelect;
         if(auto != undefined)
         {
           this.autoSelect = auto;
         }

         this.updateNegoziList();
      }
    );
  }

  ngAfterViewInit(){
  }

  updateNegoziList()
  {
    this.SvcService.getNegozi(this.userInfo['IDUTENTE'],this.marchioInfo['CODICE'],this.city,"").subscribe(data=>{
      var success = (data["SUCCESS"]!=undefined?new Boolean(data["SUCCESS"]):false);
        if(success==true)
        {
          let Info = data["DATA"][0];
          this.arNegozi = Info['ROWS'];
          if(this.autoSelect && this.city == 'Biella' && this.arNegozi.length == 1)
          {
            if(this.arNegozi[0]['IDNEGOZIO'] != undefined)
            {

              setTimeout(()=>{
              this.router.navigate(["/","selezione-tipologia"],
                {
                  replaceUrl:true, 
                  queryParams:{
                  data:JSON.stringify(
                    {
                      "user":this.userInfo,
                      "marchio":this.marchioInfo,
                      "negozio":this.arNegozi[0]
                    })
                  }
                });
              },550);
            }
          }
        }
    })
  }

  searchByKeyword(event: any){
    // console.dir(event);
    if(event.type == 'search')
      this.city = event.target.value;

    this.updateNegoziList();
  }

  getFullAddress(item:any)
  {
    let strFullAddress = "";
    if(item['INDIRIZZO'] != undefined && item['INDIRIZZO']!='')
      strFullAddress += item['INDIRIZZO'];
    
    if(item['FRAZIONE'] != undefined && item['FRAZIONE']!='')
      strFullAddress += " fraz. " + item['FRAZIONE'];
    
    if(item['CAP'] != undefined && item['CAP']!='')
      strFullAddress += " " + item['CAP'];

    if(item['LOCALITA'] != undefined && item['LOCALITA']!='')
      strFullAddress += " " + item['LOCALITA'];
    
    if(item['PROVINCIA_CODICE'] != undefined && item['PROVINCIA_CODICE']!='')
      strFullAddress += " " + item['PROVINCIA_CODICE'];
    
    if(item['NAZIONE_DESCRIZIONE'] != undefined && item['NAZIONE_DESCRIZIONE']!='')
      strFullAddress += " " + item['NAZIONE_DESCRIZIONE'];

    return  strFullAddress;
  }

  goToAppuntamenti(item)
  {
    if(item['IDNEGOZIO'] != undefined)
    {
      this.router.navigate(["/","selezione-tipologia"],
        {
          replaceUrl:true, 
          queryParams:{
          data:JSON.stringify(
            {
              "user":this.userInfo,
              "marchio":this.marchioInfo,
              "negozio":item
            })
          }
        });
    }
  }

  back()
  {
    this.router.navigate(["/",'selezione-marchio'],
    {
      replaceUrl:true, 
      queryParams:{
        data:JSON.stringify(
        {
          "user":this.userInfo
        })
      }
    });
  }
  
  openSideMenu(){
    this.menu.open('side')
  }

}
