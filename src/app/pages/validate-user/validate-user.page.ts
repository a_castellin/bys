import { Component, OnInit, AfterViewInit } from '@angular/core';
import { SvcRequestsService } from '../../services/svc-requests.service';
import { from } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { Platform } from '@ionic/angular';
import * as Constants from '../../constants'
import { MyUtils } from '../../services/my-utils.service'

@Component({
  selector: 'app-validate-user',
  templateUrl: './validate-user.page.html',
  styleUrls: ['./validate-user.page.scss'],
})
export class ValidateUserPage implements OnInit, AfterViewInit {
  userInfo:any;
  validationNumber:string;

  constructor(
    private router:Router, 
    private activatedRoute:ActivatedRoute, 
    private SvcService:SvcRequestsService, 
    private alertController:AlertController,
    private storage:Storage,
    private platform:Platform,
    private myUtils:MyUtils) { }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(
      params => {
        let requestData = JSON.parse(params['data']);
        this.userInfo = requestData.user;
      }
    );
  }

  ngAfterViewInit(){
  }

  cancel(){
    this.router.navigate(["/","login"],{replaceUrl:true});
  }

  validate(){
    if(this.validationNumber != ''){
      this.SvcService.validateUser(this.userInfo['IDUTENTE'],this.validationNumber).subscribe(data=>{
        var success = (data["SUCCESS"]!=undefined?new Boolean(data["SUCCESS"]):false);
        if(success==true)
        {
          this.ValidationCompleteAlert()
        }
        else
        {
          if(data["ERRORCODE"] == 107)
          {
            this.ValidationWrongAlert(data['ERRORMESSAGE']);
          }
          else
          {
            this.ValidationErrorAlert();
          }
        }
      }
      ,error => {
        this.ValidationErrorAlert();
      });
    }
    else
    {
      this.MissingParamAlert();
    }
  }

  sendNewCode(){
    this.sendNewCodeAlert();
  }

  async ValidationCompleteAlert() {
    const alert = await this.alertController.create({
      header: '',
      message: 'Verifica avvenuta con successo.',
      buttons: [{
        text: 'Ok',
        handler: () => {
          this.router.navigate(['/','elenco-appuntamenti'],{replaceUrl:true, queryParams:{data:JSON.stringify({"user":this.userInfo})}});
        }
      }],
      mode:"ios"
    });

    await alert.present();
  }

  async ValidationErrorAlert() {
    const alert = await this.alertController.create({
      header: 'Attenzione!',
      message: 'Si sono verificati dei problemi in fase di verifica.',
      buttons: ['Ok'],
      mode:"ios"
    });

    await alert.present();
  }

  async ValidationWrongAlert(error:string) {
    const alert = await this.alertController.create({
      header: 'Attenzione',
      message: error,
      buttons: ['OK'],
      mode:"ios"
    });

    await alert.present();
  }

  async MissingParamAlert() {
    const alert = await this.alertController.create({
      header: 'Attenzione!',
      message: 'Inserire il codice di verifica ricevuto tramite SMS.',
      buttons: ['OK'],
      mode:"ios"
    });

    await alert.present();
  }

  async sendNewCodeAlert() {
    const alert = await this.alertController.create({
      header: '',
      message: "Confermi di voler procedere alla richiesta di un nuovo codice di verifica?",
      buttons: [{
        text: 'Ok',
        handler: () => {
          this.SvcService.sendNewCode(this.userInfo['IDUTENTE']).subscribe(data=>{
            var success = (data["SUCCESS"]!=undefined?new Boolean(data["SUCCESS"]):false);
              if(success==true)
              {
                this.sendNewCodeOK();
              }
              else
              {
                if(data["ERRORCODE"] == 106)
                {
                  this.sendNewCodeLimit();
                }
                else
                {
                  this.sendNewCodeKO();
                }               
              }
          },
          error=>{
            this.sendNewCodeKO();
          });
        }        
      },
      {
        text: 'Cancel'
      }],
      mode:"ios"
    });

    await alert.present();
  }

  async sendNewCodeOK() {
    const alert = await this.alertController.create({
      header: '',
      message: "La richiesta è avvenuta con successo.<br>A breve riceverà un nuovo codice.",
      buttons: ['OK'],
      mode:"ios"
    });

    await alert.present();
  }

  async sendNewCodeLimit() {
    const alert = await this.alertController.create({
      header: '',
      message: "Superato il limite massimo di richieste per un nuovo codice.<br>In caso di necessità contattare il servizio clienti.<br><a href='" + Constants.ASSISTENZA + "' target='_blank'>assisatenza@capthasystems.com</a>",
      buttons: ['OK'],
      mode:"ios"
    });

    await alert.present();
  }

  async sendNewCodeKO() {
    const alert = await this.alertController.create({
      header: '',
      message: "Si sono verificati dei problemi durante la generazione di un nuovo codice",
      buttons: ['OK'],
      mode:"ios"
    });

    await alert.present();
  }
  


}
