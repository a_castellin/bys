import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DettaglioArticoloPage } from './dettaglio-articolo.page';

const routes: Routes = [
  {
    path: '',
    component: DettaglioArticoloPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DettaglioArticoloPageRoutingModule {}
