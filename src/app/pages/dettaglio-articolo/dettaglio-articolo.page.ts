import { Component, OnInit } from '@angular/core';
import { SvcRequestsService } from '../../services/svc-requests.service';
import { from } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import * as Constants from '../../constants'
import { MenuController } from '@ionic/angular';
import { Platform } from '@ionic/angular';
import { ModalController } from '@ionic/angular';
import { ImageModPage } from '../../modal/image-mod/image-mod.page'
import { MyUtils } from '../../services/my-utils.service'

@Component({
  selector: 'app-dettaglio-articolo',
  templateUrl: './dettaglio-articolo.page.html',
  styleUrls: ['./dettaglio-articolo.page.scss'],
})
export class DettaglioArticoloPage implements OnInit {
  userInfo: any;
  marchioInfo: any;
  negozioInfo: any;
  appuntamentoInfo:any;
  articoloInfo:any;
  barcode:string;
  arTaglieValide:any[] = [];
  qta:number = 0;
  qtaSelezionata:number = 0;
  tipoAppuntamento:string = '';
  
  constructor(
    private platform: Platform,
    private router:Router, 
    private activatedRoute:ActivatedRoute, 
    private SvcService:SvcRequestsService, 
    private alertController:AlertController,
    private storage:Storage,
    private modalController:ModalController,
    private menu:MenuController,
    private myUtils:MyUtils
  ) { }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(
      params => {
        let requestData = JSON.parse(params['data']);
        this.userInfo = requestData.user;
        this.marchioInfo = requestData.marchio;
        this.negozioInfo = requestData.negozio;
        this.appuntamentoInfo = requestData.appuntamento;
        this.articoloInfo = requestData.articolo;
        this.barcode = requestData.barcode;
        
        this.tipoAppuntamento = this.appuntamentoInfo['TIPOAP_CODICE'].toString();
        this.qta = this.articoloInfo['QTA'].toString();

        if(this.tipoAppuntamento == '5') //Cambio Taglia
        {
          this.updateTaglieValide();
        }

      }
    );
  }

  getImageUrl(item)
  {
    return item['ARTICOLO_IMAGE'];
  }

  getImageUrlMarchio(item)
  {
    // return Constants.IMAGE_BASE_URL + item['IMG'];
    return item['IMG'];
  }

  updateTaglieValide()
  {
    this.SvcService.getTaglieValide(
      this.userInfo['IDUTENTE'],
      this.marchioInfo['CODICE'],
      this.negozioInfo['IDNEGOZIO'],
      this.articoloInfo['ARTICOLO_CODICE'],
      this.articoloInfo['COLORE_CODICE'],
      this.articoloInfo['TAGLIA_POSIZIONE']).subscribe(data=>{
      var success = (data["SUCCESS"]!=undefined?new Boolean(data["SUCCESS"]):false);
        if(success==true)
        {
          //console.dir(data);
          this.arTaglieValide = data['DATA'];
          this.arTaglieValide = this.arTaglieValide.sort((a,b) => { return a['TAGLIA_POSIZIONE'] - b['TAGLIA_POSIZIONE']})
        }
    })
  }

  back(){
    this.router.navigate(["/","selezione-articolo"],
    {
      replaceUrl:true, 
      queryParams:{
        data:JSON.stringify(
        {
          "user":this.userInfo,
          "marchio":this.marchioInfo,
          "negozio":this.negozioInfo,
          "appuntamento":this.appuntamentoInfo,
          "barcode":this.barcode
        })
      }
    });
  }

  update(){
    if(this.qtaSelezionata == 0)
    {
      this.noArticoli()
    }
    else
    {
      this.updateArticoli()
    }

  }
  clear(){
    this.router.navigate(["/","elenco-articoli"],
    {
      replaceUrl:true, 
      queryParams:{
        data:JSON.stringify(
        {
          "user":this.userInfo,
          "marchio":this.marchioInfo,
          "negozio":this.negozioInfo,
          "appuntamento":this.appuntamentoInfo
        })
      }
    });
  }

  addItem(item?:any)
  {
    if(this.tipoAppuntamento == '4')
    {
      if(this.qtaSelezionata < this.qta)
      {
        this.qtaSelezionata++
      }
      else
      {
        this.superatoScontrino()
      }
    }
    else if(this.tipoAppuntamento == '5')
    {
      if(item != undefined && item['QTA']!=undefined && item['QTA_DISPONIBILE']!=undefined)
      {
        if(item['QTA'] < item['QTA_DISPONIBILE'])
        {
          if(this.qtaSelezionata < this.qta)
          {
            this.qtaSelezionata++;
            item['QTA']++;
          }
          else
          {
            this.superatoScontrino();
          }
        }
        else
        {
          this.superataDisponibilita();
        }
      }
    }
  }
  removeItem(item?:any)
  {
    if(this.tipoAppuntamento == '4')
    {
      if(this.qtaSelezionata > 0)
      {
        this.qtaSelezionata--
      }
    }
    else if(this.tipoAppuntamento == '5')
    {
      if(item != undefined && item['QTA']!=undefined && item['QTA_DISPONIBILE']!=undefined)
      {
        if(item['QTA'] > 0 && this.qtaSelezionata > 0)
        {
          this.qtaSelezionata--;
          item['QTA']--;
        }
      }
    }
  }
  async superataDisponibilita() {
    const alert = await this.alertController.create({
      header: 'Attenzione!',
      message: "Non è possibile selezionare più articoli rispetto a quelli disponibili",
      buttons: ['Ok'],
      mode:"ios"
    });

    await alert.present();
  }

  async superatoScontrino() {
    const alert = await this.alertController.create({
      header: 'Attenzione!',
      message: "Non è possibile selezionare più articoli rispetto a quelli acquistati",
      buttons: ['Ok'],
      mode:"ios"
    });

    await alert.present();
  }

  async noArticoli() {
    const alert = await this.alertController.create({
      header: 'Attenzione!',
      message: "Per confermare l'operazione è necessario selezionare almeno un articolo",
      buttons: ['Ok'],
      mode:"ios"
    });

    await alert.present();
  }

  async updateArticoli() {
    const alert = await this.alertController.create({
      header: '',
      message: "Desideri confermare gli articoli selezionati?",
      buttons: [{
        text: 'Ok',
        handler: () => {
          this.SvcService.saveCambioReso(
            this.userInfo,
            this.marchioInfo,
            this.negozioInfo,
            this.appuntamentoInfo,
            this.articoloInfo,
            this.barcode,
            this.qtaSelezionata,
            this.arTaglieValide
            ).subscribe(
              data=>{
              var success = (data["SUCCESS"]!=undefined?new Boolean(data["SUCCESS"]):false);
                if(success==true)
                {
                  this.updateArticoliOK();
                }
                else
                {
                  this.updateArticoliKO();
                }
              },
              error=>{
                this.updateArticoliKO();
              });
        }        
      },
      {
        text: 'Cancel'
      }],
      mode:"ios"
    });

    await alert.present();
  }

  async updateArticoliOK() {
    const alert = await this.alertController.create({
      header: '',
      message: "Caricamento avvenuto con successo.<br>Si desidera caricare altri articoli dallo scontrino corrente?",
      buttons: [
        {
          text:'Sì',
          handler:()=>{
            this.back();
          }
        },
        {
          text:'No',
          handler:()=>{
            this.clear();
          }
        }],
      mode:"ios"
    });

    await alert.present();
  }

  async updateArticoliKO() {
    const alert = await this.alertController.create({
      header: 'Attenzione',
      message: "Si sono verificati dei problemi durante l'invio della richiesta'!",
      buttons: ['Ok'],
      mode:"ios"
    });

    await alert.present();
  }

  getTipologia()
  {
    switch(this.appuntamentoInfo['TIPOAP_CODICE'].toString())
    {
      case '4':
        {
          return "Cambio Merce";
        }
      case '5':
        {
          return "Cambio Taglia";
        }   
    }
  }

  async presentModal(item:any) {
    const modal = await this.modalController.create({
      component: ImageModPage,
      cssClass: 'modal-image-component',
      componentProps: {
        'imgAbsPath': this.getImageUrl(item)
      }
    });
    return await modal.present();
  }
}
