import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DettaglioArticoloPageRoutingModule } from './dettaglio-articolo-routing.module';

import { DettaglioArticoloPage } from './dettaglio-articolo.page';
import { ImageModPageModule } from '../../modal/image-mod/image-mod.module'
import { ComponentsModule } from '../../components/components.module'

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DettaglioArticoloPageRoutingModule,
    ComponentsModule,
    ImageModPageModule
  ],
  declarations: [DettaglioArticoloPage]
})
export class DettaglioArticoloPageModule {}
