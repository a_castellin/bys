import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SelezioneArticoloPageRoutingModule } from './selezione-articolo-routing.module';

import { SelezioneArticoloPage } from './selezione-articolo.page';
import { ComponentsModule } from '../../components/components.module'
import { ImageModPageModule } from '../../modal/image-mod/image-mod.module'

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SelezioneArticoloPageRoutingModule,
    ComponentsModule,
    ImageModPageModule
  ],
  declarations: [SelezioneArticoloPage]
})
export class SelezioneArticoloPageModule {}
