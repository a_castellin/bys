import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SelezioneArticoloPage } from './selezione-articolo.page';

describe('SelezioneArticoloPage', () => {
  let component: SelezioneArticoloPage;
  let fixture: ComponentFixture<SelezioneArticoloPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelezioneArticoloPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SelezioneArticoloPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
