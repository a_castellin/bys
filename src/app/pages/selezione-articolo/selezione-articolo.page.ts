import { Component, OnInit} from '@angular/core';
import { SvcRequestsService } from '../../services/svc-requests.service';
import { from } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import * as Constants from '../../constants'
import { MenuController } from '@ionic/angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { ModalController } from '@ionic/angular';
import { ImageModPage } from '../../modal/image-mod/image-mod.page'
import { MyUtils } from '../../services/my-utils.service'

@Component({
  selector: 'app-selezione-articolo',
  templateUrl: './selezione-articolo.page.html',
  styleUrls: ['./selezione-articolo.page.scss'],
})
export class SelezioneArticoloPage implements OnInit {
  userInfo: any;
  marchioInfo: any;
  negozioInfo: any;
  appuntamentoInfo:any;
  barcode:string;
  arArticoli: any[]=[];
  constructor(
    private router: Router, 
    private activatedRoute: ActivatedRoute, 
    private SvcService: SvcRequestsService, 
    private alertController: AlertController,
    private menu:MenuController,
    private modalController:ModalController,
    private barcodeScanner:BarcodeScanner,
    private myUtils:MyUtils
    ) { }


  ngOnInit() {

    this.activatedRoute.queryParams.subscribe(
      params => {
        let requestData = JSON.parse(params['data']);
        this.userInfo = requestData.user;
        this.marchioInfo = requestData.marchio;
        this.negozioInfo = requestData.negozio;
        this.appuntamentoInfo = requestData.appuntamento;
        this.barcode = requestData.barcode;
        
        this.updateArticoliList();
      }
    );
  }



  updateArticoliList(){
    let dtData = new Date(this.appuntamentoInfo['DATA_STR']);
    let strDataOra = dtData.getFullYear()+""+(dtData.getMonth() + 1).toString().padStart(2,"0")+""+dtData.getDate().toString().padStart(2,"0") + "/" + this.appuntamentoInfo['ORA_STR'];
     
    this.SvcService.getDettaglioScontrino(this.userInfo['IDUTENTE'],this.marchioInfo['CODICE'],this.negozioInfo['IDNEGOZIO'],this.barcode,strDataOra).subscribe(data=>{
      var success = (data["SUCCESS"]!=undefined?new Boolean(data["SUCCESS"]):false);
        if(success==true)
        {
          this.arArticoli = data['DATA'];
          if(this.arArticoli.length == 0)
          {
            this.noArticoli();
          }
        }
        else
        {
            // this.DefualtAlert("Attenzione!",data["ERRORMESSAGE"]);
            this.DefualtAlert("Attenzione!","Si sono verificati errori durante la lettura del barcode")
        }
    },
    error=>{this.DefualtAlert("Attenzione!","Si sono verificati errori durante la lettura del barcode")}
    )
  }

  back()
  {
    this.router.navigate(["/","elenco-scontrini"],
    {
      replaceUrl:true, 
      queryParams:{
        data:JSON.stringify(
        {
          "user":this.userInfo,
          "marchio":this.marchioInfo,
          "negozio":this.negozioInfo,
          "appuntamento":this.appuntamentoInfo,
        })
      }
    });
  }

  goToArticolo(item:any)
  {
    this.router.navigate(["/","dettaglio-articolo"],
    {
      replaceUrl:true, 
      queryParams:{
        data:JSON.stringify(
        {
          "user":this.userInfo,
          "marchio":this.marchioInfo,
          "negozio":this.negozioInfo,
          "appuntamento":this.appuntamentoInfo,
          "articolo":item,
          "barcode":this.barcode
        })
      }
    });
  }
  
  getImageUrl(item)
  {
    return item['ARTICOLO_IMAGE'];
  }

  async DefualtAlert(header:string, message:string) {
    const alert = await this.alertController.create({
      header: header,
      message: message,
      buttons: ['OK'],
      mode:"ios"
    });

    await alert.present();
  }

  async noArticoli() {
    const alert = await this.alertController.create({
      header: "Attenzione!",
      message: "Non sono presenti articoli selezionabili per lo scontrino N° " + this.barcode,
      buttons: [
        {
          text:'OK',
          handler:()=>{
            this.back();
          }
        }],
      mode:"ios"
    });

    await alert.present();
  }

  getTipologia()
  {
    switch(this.appuntamentoInfo['TIPOAP_CODICE'].toString())
    {
      case '4':
        {
          return "Cambio Merce";
        }
      case '5':
        {
          return "Cambio Taglia";
        }   
    }
  }

  showImage(event:Event, item:any)
  {
    event.preventDefault();
    event.stopPropagation();
    this.presentModal(item);
  }

  async presentModal(item:any) {
    const modal = await this.modalController.create({
      component: ImageModPage,
      cssClass: 'modal-image-component',
      componentProps: {
        'imgAbsPath': this.getImageUrl(item)
      }
    });
    return await modal.present();
  }

}
