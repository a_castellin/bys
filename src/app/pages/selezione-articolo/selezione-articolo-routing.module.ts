import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SelezioneArticoloPage } from './selezione-articolo.page';

const routes: Routes = [
  {
    path: '',
    component: SelezioneArticoloPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SelezioneArticoloPageRoutingModule {}
