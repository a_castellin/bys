import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ElencoScontriniPage } from './elenco-scontrini.page';

describe('ElencoScontriniPage', () => {
  let component: ElencoScontriniPage;
  let fixture: ComponentFixture<ElencoScontriniPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElencoScontriniPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ElencoScontriniPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
