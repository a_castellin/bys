import { Component, OnInit} from '@angular/core';
import { SvcRequestsService } from '../../services/svc-requests.service';
import { from } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import * as Constants from '../../constants'
import { MenuController } from '@ionic/angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { MyUtils } from '../../services/my-utils.service'

@Component({
  selector: 'app-elenco-scontrini',
  templateUrl: './elenco-scontrini.page.html',
  styleUrls: ['./elenco-scontrini.page.scss'],
})
export class ElencoScontriniPage implements OnInit {
  userInfo: any;
  marchioInfo: any;
  negozioInfo: any;
  appuntamentoInfo:any;
  arScontrini: any[]=[];
  constructor(
    private router: Router, 
    private activatedRoute: ActivatedRoute, 
    private SvcService: SvcRequestsService, 
    private alertController: AlertController,
    private menu:MenuController,
    private barcodeScanner:BarcodeScanner,
    private myUtils:MyUtils
    ) { }

  Months: string[]=["Gen","Feb","Mar","Apr","Mag","Giu","Lug","Ago","Set","Ott","Nov","Dec"];
  Days: string[]=["Dom","Lun","Mar","Mer","Gio","Ven","Sab"];
  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(
      params => {
        let requestData = JSON.parse(params['data']);
        this.userInfo = requestData.user;
        this.marchioInfo = requestData.marchio;
        this.negozioInfo = requestData.negozio;
        this.appuntamentoInfo = requestData.appuntamento;
        
        this.updateScontriniList();
      }
    );
  }

  updateScontriniList(){
    let dtData = new Date(this.appuntamentoInfo['DATA_STR']);
    let strDataOra = dtData.getFullYear()+""+(dtData.getMonth() + 1).toString().padStart(2,"0")+""+dtData.getDate().toString().padStart(2,"0") + "/" + this.appuntamentoInfo['ORA_STR'];
     
    this.SvcService.getScontrini(this.userInfo['IDUTENTE'],this.marchioInfo['CODICE'],this.negozioInfo['IDNEGOZIO'],strDataOra).subscribe(data=>{
      var success = (data["SUCCESS"]!=undefined?new Boolean(data["SUCCESS"]):false);
        if(success==true)
        {
          this.arScontrini = data['DATA'];
          
          this.arScontrini = this.arScontrini.sort((a,b)=>{
            if(a['DATA_SCONTRINO'] < b['DATA_SCONTRINO'])
              return -1;
            if(a['DATA_SCONTRINO'] > b['DATA_SCONTRINO'])
              return 1;
            return 0;
          });
        }
    })
  }

  getData(dataScontrino:string)
  {
    let dateStr = dataScontrino.substr(0,4) + '-' + dataScontrino.substr(4,2) + '-' + dataScontrino.substr(6,2);
    let CurrentDate = new Date(dateStr);
    return this.Days[CurrentDate.getDay()] + " " + CurrentDate.getDate() + " " + this.Months[CurrentDate.getMonth()] + " " + CurrentDate.getFullYear();
  }

  scanBarcode()
  {
    this.barcodeScanner.scan(
      {
        preferFrontCamera : false, // iOS and Android
        showFlipCameraButton : true, // iOS and Android
        showTorchButton : true, // iOS and Android
        torchOn: false, // Android, launch with the torch switched on (if available)
        prompt : "Inquadrare il barcode dello scontrino", // Android
        resultDisplayDuration: 500, // Android, display scanned text for X ms. 0 suppresses it entirely, default 1500
        // formats : "QR_CODE,PDF_417", // default: all but PDF_417 and RSS_EXPANDED
        // orientation : "landscape", // Android only (portrait|landscape), default unset so it rotates with the device
        disableAnimations : true, // iOS
        disableSuccessBeep: false // iOS and Android
    }
    ).then(result => {
      if(result != undefined)
      {
        if(!result.cancelled)
        {
          if(result.text != '')
          {
            this.goToScontrino(result.text);
          }
        }
      }

      // alert("We got a barcode\n" +
      //       "Result: " + result.text + "\n" +
      //       "Format: " + result.format + "\n" +
      //       "Cancelled: " + result.cancelled);

      },
      error => {
      },
    );
  }

  back()
  {
    this.router.navigate(["/","elenco-articoli"],
    {
      replaceUrl:true, 
      queryParams:{
        data:JSON.stringify(
        {
          "user":this.userInfo,
          "marchio":this.marchioInfo,
          "negozio":this.negozioInfo,
          "appuntamento":this.appuntamentoInfo,
        })
      }
    });
  }

  goToScontrino(barcode:string)
  {
    this.router.navigate(["/","selezione-articolo"],
    {
      replaceUrl:true, 
      queryParams:{
        data:JSON.stringify(
        {
          "user":this.userInfo,
          "marchio":this.marchioInfo,
          "negozio":this.negozioInfo,
          "appuntamento":this.appuntamentoInfo,
          "barcode":barcode
        })
      }
    });
  }

  getTipologia()
  {
    switch(this.appuntamentoInfo['TIPOAP_CODICE'].toString())
    {
      case '4':
        {
          return "Cambio Merce";
        }
      case '5':
        {
          return "Cambio Taglia";
        }   
    }
  }

}
