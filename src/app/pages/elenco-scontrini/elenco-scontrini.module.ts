import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ElencoScontriniPageRoutingModule } from './elenco-scontrini-routing.module';

import { ElencoScontriniPage } from './elenco-scontrini.page';
import { ComponentsModule } from '../../components/components.module'

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ElencoScontriniPageRoutingModule,
    ComponentsModule
  ],
  declarations: [ElencoScontriniPage]
})
export class ElencoScontriniPageModule {}
