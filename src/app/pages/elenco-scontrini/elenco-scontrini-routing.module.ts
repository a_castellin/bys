import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ElencoScontriniPage } from './elenco-scontrini.page';

const routes: Routes = [
  {
    path: '',
    component: ElencoScontriniPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ElencoScontriniPageRoutingModule {}
