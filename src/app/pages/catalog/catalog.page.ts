import { Component, OnInit, ErrorHandler} from '@angular/core';
import { SvcRequestsService } from '../../services/svc-requests.service';
import { from } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import * as Constants from '../../constants'
import { MenuController } from '@ionic/angular';
import { MyUtils } from '../../services/my-utils.service'

@Component({
  selector: 'app-catalog',
  templateUrl: './catalog.page.html',
  styleUrls: ['./catalog.page.scss'],
})
export class CatalogPage implements OnInit {
  arCataloghi: any[]=[];
  userInfo: any;
  marchioInfo: any;
  negozioInfo: any;
  constructor(
    private router: Router, 
    private activatedRoute: ActivatedRoute, 
    private SvcService: SvcRequestsService, 
    private alertController: AlertController,
    private menu:MenuController,
    private myUtils:MyUtils
  ) { }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(
      params => {
        let requestData = JSON.parse(params['data']);
        this.userInfo = requestData.user;
        this.marchioInfo = requestData.marchio;
        this.negozioInfo = requestData.negozio;
        this.updateCataloghi();
      }
    );
  }

  updateCataloghi(){
    this.arCataloghi = [];

    this.SvcService.getCataloghi(this.userInfo['IDUTENTE']).subscribe(data=>{
      var success = (data["SUCCESS"]!=undefined?new Boolean(data["SUCCESS"]):false);
        if(success==true)
        {
          // console.dir(data["DATA"]);
          this.arCataloghi = data["DATA"];
        }
    })
  }

  getElencoAziende()
  {
    let arAziende = [];
    let map = new Map()
    for(let item of this.arCataloghi)
    {
      if(!map.has(item['IDAZIENDA']))
      {
        map.set(item['IDAZIENDA'],true);
        arAziende.push({
          "IDAZIENDA":item["IDAZIENDA"],
          "IMG":item["LOGO"],
          "DESCRIZIONE":item["AZIENDA_DESC"]
        });
      }
    }

    return arAziende;
  }

  getImageUrl(item)
  {
    // return Constants.IMAGE_BASE_URL + item['IMG'];
    return item['IMG'];
  }

  goToURL(item:any)
  {
    if(item['CATALOGO_HREF']!=undefined && item['CATALOGO_HREF']!='')
      this.myUtils.goToUrl(item['CATALOGO_HREF']);
      // window.open(item['CATALOGO_HREF'], '_system', 'location=yes');
  }

  getCataloghi(azienda:any)
  {
    return this.arCataloghi.filter(item=>{return item['IDAZIENDA'] == azienda['IDAZIENDA']});
  }

  logOut()
  {
    this.router.navigate(["/","login"],{replaceUrl:true});
  }

  openSideMenu(){
    this.menu.open('side')
  }
}
