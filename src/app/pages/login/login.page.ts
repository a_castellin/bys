import { Component, OnInit} from '@angular/core';
import { SvcRequestsService } from '../../services/svc-requests.service';
import { from } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { Platform } from '@ionic/angular';
import { MyUtils } from '../../services/my-utils.service'
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit{
  nome:string = '';
  numero:string = ''
  username: string ='';
  password: string = '';

  constructor(
    private router:Router, 
    private activatedRoute:ActivatedRoute, 
    private SvcService:SvcRequestsService, 
    private alertController:AlertController,
    private storage:Storage,
    private platform:Platform,
    private myUtils:MyUtils) { }

  ngOnInit() {
    this.storage.get("nome").then((val) => {
      this.nome = val;
    });
    this.storage.get("numero").then((val) => {
      this.numero = val;
    });
    this.storage.get("password").then((val) => {
      this.password = val;
    });
  }

  registerNewUser()
  {
    this.router.navigate(['/','register-new-user'],{replaceUrl:true});
  }

  passwordRecovery()
  {
    this.router.navigate(['/','password-recovery'],{replaceUrl:true});
  }

  login()
  {
    if(this.nome!='' && this.numero!='' && this.password!=''){
      this.SvcService.login(this.nome,this.numero,this.password).subscribe(data=>{
        var success = (data["SUCCESS"]!=undefined?new Boolean(data["SUCCESS"]):false);
        if(success==true)
        {
          let userInfo = data["DATA"][0];
          this.storage.set("nome",this.nome);
          this.storage.set("numero",this.numero);
          this.storage.set("password",this.password);
          this.SvcService.setFCMToken(userInfo['IDUTENTE']);
          
          if(userInfo['ABILITATO'] != undefined && (Number)(userInfo['ABILITATO']) > 0)
          {
            if(userInfo['FORCE_CHANGE']!= undefined && (Number)(userInfo['FORCE_CHANGE']) == 0)
            {
              this.router.navigate(['/','elenco-appuntamenti'],{replaceUrl:true, queryParams:{data:JSON.stringify({"user":userInfo})}});
            }
            else
            {
              this.router.navigate(['/','change-password'],{replaceUrl:true, queryParams:{data:JSON.stringify({"user":userInfo, "back":"login"})}});
            }
          }
          else
          {
            this.router.navigate(['/','validate-user'],{replaceUrl:true, queryParams:{data:JSON.stringify({"user":userInfo})}});
          }  
        }
        else
        {
          this.LoginFailedAlert();
        }
      },
      error=>{
        
      });
    }
    else
    {
      this.MissingParamAlert();
    }
  }

  async LoginFailedAlert() {
    const alert = await this.alertController.create({
      header: 'Attenzione!',
      message: 'Username o Passwrod errati',
      buttons: ['OK'],
      mode:"ios"
    });

    await alert.present();
  }

  async MissingParamAlert() {
    const alert = await this.alertController.create({
      header: 'Attenzione!',
      message: 'Inserire tutti i valori richiesti',
      buttons: ['OK'],
      mode:"ios"
    });

    await alert.present();
  }



}
