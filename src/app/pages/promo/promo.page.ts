import { Component, OnInit, ViewChild, ViewChildren, QueryList} from '@angular/core';
import { SvcRequestsService } from '../../services/svc-requests.service';
import { from } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import * as Constants from '../../constants'
import { MenuController } from '@ionic/angular';
import { IonSlides } from '@ionic/angular';
import { ModalController } from '@ionic/angular';
import { IonRouterOutlet } from '@ionic/angular';
import { ImageModPage } from '../../modal/image-mod/image-mod.page'
import { MyUtils } from '../../services/my-utils.service'

@Component({
  selector: 'app-promo',
  templateUrl: './promo.page.html',
  styleUrls: ['./promo.page.scss'],
})
export class PromoPage implements OnInit {
  arPromo: any[]=[];
  userInfo: any;
  marchioInfo: any;
  negozioInfo: any;
  city: string;
  constructor(
    private router: Router, 
    private activatedRoute: ActivatedRoute, 
    private SvcService: SvcRequestsService, 
    private alertController: AlertController,
    private menu:MenuController,
    private modalController:ModalController,
    private routerOutlet: IonRouterOutlet,
    private myUtils:MyUtils) { }

  Months: string[]=["Gen","Feb","Mar","Apr","Mag","Giu","Lug","Ago","Set","Ott","Nov","Dec"];
  Days: string[]=["Dom","Lun","Mar","Mer","Gio","Ven","Sab"];
  loading:boolean = true;

  slideOptions: any = {
    // autoplay: {
    //   delay: 5000,
    // },
    loop:false
  }
  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(
      params => {
        let requestData = JSON.parse(params['data']);
        this.userInfo = requestData.user;
        this.marchioInfo = requestData.marchio;
        this.negozioInfo = requestData.negozio;
        this.updatePromoList();
      }
    );
    // this.loadData();
  }

  loadData()
  {
    const promise = new Promise((resolve, reject) => {
      this.activatedRoute.queryParams.subscribe(
        params => {
          let requestData = JSON.parse(params['data']);
          this.userInfo = requestData.user;
          this.marchioInfo = requestData.marchio;
          this.negozioInfo = requestData.negozio;
          this.updatePromoList();
          resolve();
        },
        error=>{
          reject();
        }
      );
    });
    return promise;
  }

  updatePromoList(){

    const promise = new Promise((resolve, reject) => {
      
    let userId = "0";
    let aziendaId = "0";
    let negozioId = "0";

    if(this.userInfo != undefined && this.userInfo['IDUTENTE']!=undefined && this.userInfo['IDUTENTE']!='')
      userId = this.userInfo['IDUTENTE'];
    
    if(this.marchioInfo != undefined && this.marchioInfo['CODICE']!=undefined && this.marchioInfo['CODICE']!='')
      aziendaId = this.marchioInfo['CODICE'];
    
    if(this.negozioInfo != undefined && this.negozioInfo['IDNEGOZIO']!=undefined && this.negozioInfo['IDNEGOZIO']!='')
      negozioId = this.negozioInfo['IDNEGOZIO'];

    
    this.SvcService.getPromo(userId.toString(), aziendaId.toString(), negozioId.toString())
    .toPromise()
    .then(data=>{
        var success = (data["SUCCESS"]!=undefined?new Boolean(data["SUCCESS"]):false);
          if(success==true)
          {
            this.arPromo = data["DATA"];
          }

          this.loading = false;
          resolve();
      },
      err => {
        // Error
        reject(err);
      })
    });
    return promise;
  }

  openSideMenu(){
    this.menu.open('side')
  }
  
  logOut()
  {
    this.router.navigate(["/","login"],{replaceUrl:true});
  }

  getImageUrl(item:any)
  {
    if(item['PROMO_IMG'] != undefined && item['PROMO_IMG']!= '')
      return item['PROMO_IMG'];
    else
      return '../../../assets/images/no-image.png';
  }

  goToURL(item:any)
  {
    if(item['PROMO_HREF']!=undefined && item['PROMO_HREF']!='')
      window.open(item['PROMO_HREF'], '_system', 'location=yes');
  }

  getMarchi()
  {
    let arAziende = [];
    let map = new Map()
    for(let item of this.arPromo)
    {
      if(!map.has(item['IDAZIENDA']))
      {
        map.set(item['IDAZIENDA'],true);
        arAziende.push(item['IDAZIENDA']);
      }
    }

    return arAziende;
  }

  getPromo(marchio:any)
  {
    return this.arPromo.filter(item=>{return item['IDAZIENDA'] == marchio});
  }

  enlargeImage(item:any)
  {
    this.presentModal(item);
  }

  async imgAlert(item:any) {
    const alert = await this.alertController.create({
      cssClass: 'EnlargeImageAlert',
      message: "<img src='" + this.getImageUrl(item) + "'>",
      buttons: ['OK'],
      mode:"ios"
    });

    await alert.present();
  }

  async presentModal(item:any) {
    const modal = await this.modalController.create({
      component: ImageModPage,
      cssClass: 'modal-image-component',
      componentProps: {
        'imgAbsPath': this.getImageUrl(item)
      }
    });
    return await modal.present();
  }
}
