import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PromoPageRoutingModule } from './promo-routing.module';

import { PromoPage } from './promo.page';
import { ComponentsModule } from '../../components/components.module'
import { ImageModPageModule } from '../../modal/image-mod/image-mod.module'


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PromoPageRoutingModule,
    ComponentsModule,
    ImageModPageModule
  ],
  declarations: [PromoPage]
})
export class PromoPageModule {}
