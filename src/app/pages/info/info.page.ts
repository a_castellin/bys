import { Component, OnInit} from '@angular/core';
import { SvcRequestsService } from '../../services/svc-requests.service';
import { from } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import * as Constants from '../../constants'
import { MyUtils } from '../../services/my-utils.service'

@Component({
  selector: 'app-info',
  templateUrl: './info.page.html',
  styleUrls: ['./info.page.scss'],
})
export class InfoPage implements OnInit {
  versione: any = Constants.VERSION;
  userInfo: any;
  constructor(private router:Router, private activatedRoute:ActivatedRoute,
    private myUtils:MyUtils) { }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(
      params => {
        let requestData = JSON.parse(params['data']);
        this.userInfo = requestData.user;
      }
    );
  }

  getPrivacyPolicy()
  {
    this.myUtils.goToUrl(Constants.IMAGE_BASE_URL + 'Documenti/Privacy_Policy.htm');
    // window.open(Constants.IMAGE_BASE_URL + 'Documenti/Privacy_Policy.htm','_system', 'location=yes');
  }

  getTermini()
  {
    this.myUtils.goToUrl(Constants.IMAGE_BASE_URL + 'Documenti/Termini.htm');
    // window.open(Constants.IMAGE_BASE_URL + 'Documenti/Termini.htm','_system', 'location=yes');
  }
  
  back(){
    this.router.navigate(['/','elenco-appuntamenti'],{replaceUrl:true, queryParams:{data:JSON.stringify({"user":this.userInfo})}});
  }

}
