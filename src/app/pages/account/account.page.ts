import { Component, OnInit } from '@angular/core';
import { SvcRequestsService } from '../../services/svc-requests.service';
import { from } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import * as Constants from '../../constants'
import { MenuController } from '@ionic/angular';
import { Platform } from '@ionic/angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { CypherService } from '../../services/cypher.service'
import { MyUtils } from '../../services/my-utils.service'

@Component({
  selector: 'app-account',
  templateUrl: './account.page.html',
  styleUrls: ['./account.page.scss'],
})
export class AccountPage implements OnInit {
  userInfo: any;
  cellulare: string;
  nome: string;
  cognome: string;
  email: string;
  data_nascita: string = '';
  today: string = new Date().toISOString();
  password: string;

  constructor(
    private platform: Platform,
    private router:Router, 
    private activatedRoute:ActivatedRoute, 
    private SvcService:SvcRequestsService, 
    private alertController:AlertController,
    private storage:Storage,
    private menu:MenuController,
    private barcodeScanner: BarcodeScanner,
    private cypher:CypherService,
    private myUtils:MyUtils
  ) { }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(
      params => {
        let requestData = JSON.parse(params['data']);
        this.userInfo = requestData.user;
        this.nome = Constants.EncryptEnable ? this.cypher.Decrypt(this.userInfo['NOME']):this.userInfo['NOME'];
        this.cognome = Constants.EncryptEnable ? this.cypher.Decrypt(this.userInfo['COGNOME']):this.userInfo['COGNOME'];
        this.cellulare = Constants.EncryptEnable ? this.cypher.Decrypt(this.userInfo['CELLULARE']):this.userInfo['CELLULARE'];
        this.email = Constants.EncryptEnable ? this.cypher.Decrypt(this.userInfo['EMAIL']):this.userInfo['EMAIL'];
        let strData:string = this.userInfo['DATA_NASCITA_STR'];
        if(strData != undefined && strData != ''){
          let strDataNascita = strData.substr(0,4) + '-' + strData.substr(4,2) + '-' + strData.substr(6,2);
          this.data_nascita = strDataNascita;
        }
      }
    );

    this.storage.get("password").then((val) => {
      this.password = val;
    });
  }

  back(){
    this.router.navigate(['/','elenco-appuntamenti'],{replaceUrl:true, queryParams:{data:JSON.stringify({"user":this.userInfo})}});
  }

  updateUser(){
    let dataStr="";
    if(this.data_nascita != undefined && this.data_nascita != ''){
      let data = new Date(this.data_nascita);
      dataStr = data.getFullYear()+""+(data.getMonth() + 1).toString().padStart(2,"0")+""+data.getDate().toString().padStart(2,"0");
    }
    this.SvcService.updateUser(this.nome, this.cognome, dataStr, this.cellulare, this.email,this.userInfo['IDUTENTE'],this.password).subscribe(data=>{
      var success = (data["SUCCESS"]!=undefined?new Boolean(data["SUCCESS"]):false);
        if(success==true)     
        {
          this.userInfo = data["DATA"][0];
          this.UpdateCompleteAlert()
        }
        else
        {
          if(data["ERRORCODE"] == 100)
          {
            this.DefualtAlert("Attenzione!",data["ERRORMESSAGE"]);
          }
          else
          {
            this.UpdateFailedAlert();
          }
        }
      });
  }

  async UpdateFailedAlert() {
    const alert = await this.alertController.create({
      header: 'Attenzione!',
      message: 'Si sono verificati problemi durante l\'aggiornamento',
      buttons: ['OK'],
      mode:"ios"
    });

    await alert.present();
  }

  async UpdateCompleteAlert() {
    const alert = await this.alertController.create({
      header: '',
      message: 'Aggiornamento avvenuto con successo',
      buttons: [{
        text: 'Ok',
        handler: () => {
          this.back();
        }
      }],
      mode:"ios"
    });

    await alert.present();
  }

  async DefualtAlert(header:string, message:string,) {
    const alert = await this.alertController.create({
      header: header,
      message: message,
      buttons: ['OK'],
      mode:"ios"
    });

    await alert.present();
  }

}
