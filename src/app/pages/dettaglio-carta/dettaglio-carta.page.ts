import { Component, OnInit } from '@angular/core';
import { MyUtils } from '../../services/my-utils.service'

@Component({
  selector: 'app-dettaglio-carta',
  templateUrl: './dettaglio-carta.page.html',
  styleUrls: ['./dettaglio-carta.page.scss'],
})
export class DettaglioCartaPage implements OnInit {

  constructor(
    private myUtils:MyUtils) { }

  ngOnInit() {
  }

}
