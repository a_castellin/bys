import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DettaglioCartaPageRoutingModule } from './dettaglio-carta-routing.module';

import { DettaglioCartaPage } from './dettaglio-carta.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DettaglioCartaPageRoutingModule
  ],
  declarations: [DettaglioCartaPage]
})
export class DettaglioCartaPageModule {}
