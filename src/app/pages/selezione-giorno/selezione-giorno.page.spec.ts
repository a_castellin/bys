import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SelezioneGiornoPage } from './selezione-giorno.page';

describe('SelezioneGiornoPage', () => {
  let component: SelezioneGiornoPage;
  let fixture: ComponentFixture<SelezioneGiornoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelezioneGiornoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SelezioneGiornoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
