import { Component, OnInit, AfterViewInit } from '@angular/core';
import { SvcRequestsService } from '../../services/svc-requests.service'
import { from } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { MenuController } from '@ionic/angular';
import { MyUtils } from '../../services/my-utils.service'


@Component({
  selector: 'app-selezione-giorno',
  templateUrl: './selezione-giorno.page.html',
  styleUrls: ['./selezione-giorno.page.scss'],
})
export class SelezioneGiornoPage implements OnInit, AfterViewInit {
  arGiorni: any[]=[];
  userInfo: any;
  marchioInfo: any;
  negozioInfo: any;
  tipologiaAppuntamento: any;
  constructor(private router: Router, private activatedRoute: ActivatedRoute, private SvcService: SvcRequestsService, private menu:MenuController,
    private myUtils:MyUtils) { }
  Months: string[]=["Gen","Feb","Mar","Apr","Mag","Giu","Lug","Ago","Set","Ott","Nov","Dec"];
  Days: string[]=["Dom","Lun","Mar","Mer","Gio","Ven","Sab"];

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(
      params => {
        let requestData = JSON.parse(params['data']);
        this.userInfo = requestData.user;
        this.marchioInfo = requestData.marchio
        this.negozioInfo = requestData.negozio
        this.tipologiaAppuntamento = requestData.tipologiaAppuntamento;
        this.updateGiorniList();
      }
    );
  }

  ngAfterViewInit(){
    
  }

  getDataString(date:string)
  {
    let CurrentDate = new Date(date);
    return this.Days[CurrentDate.getDay()] + " " + CurrentDate.getDate().toString().padStart(2,'0') + " " + this.Months[CurrentDate.getMonth()] + " " + CurrentDate.getFullYear();
  }

  updateGiorniList()
  {
    this.SvcService.getGiorni(this.userInfo['IDUTENTE'],this.marchioInfo['CODICE'],this.negozioInfo['IDNEGOZIO']).subscribe(data=>{
      var success = (data["SUCCESS"]!=undefined?new Boolean(data["SUCCESS"]):false);
        if(success==true)
        {
          this.arGiorni = data["DATA"];

          //DA VALUTARE SE NECESSARIO RIFORMATTARE DATA
          for(let i=0 ; i<this.arGiorni.length; i++)
          {
            let item = this.arGiorni[i];
            let strData:string = item['DATA_GIORNO'];
            item['DATA_STR'] = strData.substr(0,4) + '-' + strData.substr(4,2) + '-' + strData.substr(6,2);
          }

          this.arGiorni = this.arGiorni.sort((a,b)=>{
            if(a['DATA_GIORNO'] < b['DATA_GIORNO'])
              return -1;
            if(a['DATA_GIORNO'] > b['DATA_GIORNO'])
              return 1;
            return 0;
          });

        }
    })
  }

  selectGiorno(item)
  {
    this.router.navigate(["/","selezione-ora"],
    {
      replaceUrl:true, 
      queryParams:{
        data:JSON.stringify(
        {
          "user":this.userInfo,
          "marchio":this.marchioInfo,
          "negozio":this.negozioInfo,
          "tipologiaAppuntamento":this.tipologiaAppuntamento,
          "data":item["DATA_GIORNO"]
        })
      }
    });
  }

  back()
  {
    this.router.navigate(["/",'selezione-tipologia'],
    {
      replaceUrl:true, 
      queryParams:{
        data:JSON.stringify(
        {
          "user":this.userInfo,
          "marchio":this.marchioInfo,
          "negozio":this.negozioInfo
        })
      }
    });
  }
  
  openSideMenu(){
    this.menu.open('side')
  }

}
