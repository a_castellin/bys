import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SelezioneGiornoPage } from './selezione-giorno.page';

const routes: Routes = [
  {
    path: '',
    component: SelezioneGiornoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SelezioneGiornoPageRoutingModule {}
