import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SelezioneGiornoPageRoutingModule } from './selezione-giorno-routing.module';

import { SelezioneGiornoPage } from './selezione-giorno.page';
import { SideMenuComponent } from '../../components/side-menu/side-menu.component'
import { ComponentsModule } from '../../components/components.module'

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SelezioneGiornoPageRoutingModule,
    ComponentsModule
  ],
  declarations: [SelezioneGiornoPage]
})
export class SelezioneGiornoPageModule {}
