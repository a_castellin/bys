import { Component, OnInit, AfterViewInit } from '@angular/core';
import { SvcRequestsService } from '../../services/svc-requests.service';
import { from } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { MyUtils } from '../../services/my-utils.service'

@Component({
  selector: 'app-password-recovery',
  templateUrl: './password-recovery.page.html',
  styleUrls: ['./password-recovery.page.scss'],
})
export class PasswordRecoveryPage implements OnInit, AfterViewInit {
  username: string = '';
  phone: string = '';
  constructor(
    private router: Router, 
    private activatedRoute: ActivatedRoute, 
    private SvcService: SvcRequestsService, 
    private alertController:AlertController,
    private storage:Storage,
    private myUtils:MyUtils) { }

  ngOnInit() {
  }

  ngAfterViewInit(){
  }


  cancel(){
    this.router.navigate(["/","login"],{replaceUrl:true});
  }

  resetHighlightClass()
  {
    let elems = document.getElementsByTagName('ion-item');
    for(let i=0; i<elems.length; i++)
    {
      elems[i].classList.remove('highlight');
    }
  }

  passwordRecovery(){
    let bOk = true;
    this.resetHighlightClass();

    if(this.username == '')
    {
      bOk = false;
      let elem = document.getElementById('nome');
        elem.classList.add('highlight');
    }

    if(this.phone == '')
    {
      bOk = false;
      let elem = document.getElementById('telefono');
        elem.classList.add('highlight');
    }

    if(bOk)
    { 
      this.SvcService.passwordRecovery(this.username, this.phone).subscribe(data=>{
        var success = (data["SUCCESS"]!=undefined?new Boolean(data["SUCCESS"]):false);
          if(success==true)
          {          
            this.recuperoPasswordCompleteAlert();
          }
          else
          {
            this.DefualtAlert("Attenzione!",data["ERRORMESSAGE"]);
          }
      });    
    }
  }

  async DefualtAlert(header:string, message:string) {
    const alert = await this.alertController.create({
      header: header,
      message: message,
      buttons: ['OK'],
      mode:"ios"
    });

    await alert.present();
  }

  async recuperoPasswordCompleteAlert() {
    const alert = await this.alertController.create({
      header: '',
      message: 'Recupero password avvenuto con successo.<br>A breve riceverà un SMS contente la nuova password.',
      buttons: [{
        text: 'Ok',
        handler: () => {
          this.router.navigate(["/","login"],{replaceUrl:true});
        }
      }],
      mode:"ios"
    });

    await alert.present();
  }

}
