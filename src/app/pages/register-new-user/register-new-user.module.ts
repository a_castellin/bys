import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RegisterNewUserPageRoutingModule } from './register-new-user-routing.module';

import { RegisterNewUserPage } from './register-new-user.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RegisterNewUserPageRoutingModule
  ],
  declarations: [RegisterNewUserPage]
})
export class RegisterNewUserPageModule {}
