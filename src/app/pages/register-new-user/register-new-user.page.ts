import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { SvcRequestsService } from '../../services/svc-requests.service'
import { from } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { IonContent } from '@ionic/angular';
import { ConvertActionBindingResult } from '@angular/compiler/src/compiler_util/expression_converter';
import { Storage } from '@ionic/storage';
import * as Constants from '../../constants'
import { utils } from 'protractor';
import { MyUtils } from '../../services/my-utils.service'

@Component({
  selector: 'app-register-new-user',
  templateUrl: './register-new-user.page.html',
  styleUrls: ['./register-new-user.page.scss'],
})
export class RegisterNewUserPage implements OnInit, AfterViewInit,AfterViewInit {
  name: string = '';
  surname: string = '';
  phone: string = '';
  username: string = '';
  password: string = '';
  confermaPassword: string = '';
  email: string = '';
  data_nascita: string = '';
  privacyIsChecked: boolean = false;
  @ViewChild(IonContent, {read: IonContent, static: false}) private myContent: any;

  constructor(private router: Router, 
    private activatedRoute: ActivatedRoute, 
    private SvcService: SvcRequestsService, 
    private alertController:AlertController,
    private storage:Storage,
    private myUtils:MyUtils) { }

  ngOnInit() {  
  }

  ngAfterViewInit(){
    this.myContent.scrollToTop();
    this.resetHighlightClass();
  }

  resetHighlightClass()
  {
    let elems = document.getElementsByTagName('ion-item');
    for(let i=0; i<elems.length; i++)
    {
      elems[i].classList.remove('highlight');
    }
  }

  cancel(){
    this.router.navigate(["/","login"],{replaceUrl:true});
  }

  registerNewUser(){
    let bOk = true;
    this.resetHighlightClass();

    if(!this.privacyIsChecked)

    if(this.name == '')
    {
      bOk = false;
      let elem = document.getElementById('Nome');
        elem.classList.add('highlight');
    }
    // if(this.surname == '')
    // {
    //   bOk = false;
    //   let elem = document.getElementById('Cognome');
    //     elem.classList.add('highlight');
    // }
    // if(this.data_nascita == '')
    // {
    //   bOk = false;
    //   let elem = document.getElementById('Data');
    //     elem.classList.add('highlight');
    // }
    // if(this.email == '')
    // {
    //   bOk = false;
    //   let elem = document.getElementById('Email');
    //     elem.classList.add('highlight');
    // }
    if(this.phone == '')
    {
      bOk = false;
      let elem = document.getElementById('Numero');
        elem.classList.add('highlight');
    }
    // if(this.username == '')
    // {
    //   bOk = false;
    //   let elem = document.getElementById('Username');
    //     elem.classList.add('highlight');
    // }
    if(this.password == '')
    {
      bOk = false;
      let elem = document.getElementById('Password');
        elem.classList.add('highlight');
    }
    if(this.confermaPassword == '')
    {
      bOk = false;
      let elem = document.getElementById('ConfermaPassword');
        elem.classList.add('highlight');
    }

    if(bOk)
    {
      if(this.password != this.confermaPassword)
      {
        let elem = document.getElementById('ConfermaPassword');
        elem.classList.add('highlight');
      }
      else
      {
        let dataStr="";
        if(this.data_nascita != ''){
          let data = new Date(this.data_nascita);
          dataStr = data.getFullYear()+""+(data.getMonth() + 1).toString().padStart(2,"0")+""+data.getDate().toString().padStart(2,"0");
        }
        this.SvcService.registerNewUser(this.name, this.surname, dataStr, this.phone, this.email, this.username, this.password).subscribe(data=>{
          var success = (data["SUCCESS"]!=undefined?new Boolean(data["SUCCESS"]):false);
            if(success==true)
            {
              this.RegistrationCompleteAlert()
            }
            else
            {
                this.DefualtAlert("Attenzione!",data["ERRORMESSAGE"]);
            }
        });
      }
    }
    else
    {
    }
  }

  async RegistrationFailedAlert() {
    const alert = await this.alertController.create({
      header: 'Attenzione!',
      message: 'Si sono verificati problemi durante la registrazione',
      buttons: ['OK'],
      mode:"ios"
    });

    await alert.present();
  }

  async RegistrationCompleteAlert() {
    const alert = await this.alertController.create({
      header: '',
      message: 'Registrazione avvenuta con successo',
      buttons: [{
        text: 'Ok',
        handler: () => {
          this.storage.set("nome",this.name);
          this.storage.set("numero",this.phone);
          this.storage.set("password",this.password);
          this.router.navigate(["/","loading"],{replaceUrl:true});
        }
      }],
      mode:"ios"
    });

    await alert.present();
  }

  async DefualtAlert(header:string, message:string,) {
    const alert = await this.alertController.create({
      header: header,
      message: message,
      buttons: ['OK'],
      mode:"ios"
    });

    await alert.present();
  }

  async MissingParamAlert(missingValue:string) {
    const alert = await this.alertController.create({
      header: 'Attenzione!',
      message: 'Alcuni campi obbligatori risultano non compilati<br>'+missingValue,
      buttons: ['OK'],
      mode:"ios"
    });

    await alert.present();
  }

  getPrivacyPolicy()
  {
    this.myUtils.goToUrl(Constants.IMAGE_BASE_URL + 'Documenti/Privacy_Policy.htm');
    // window.open(Constants.IMAGE_BASE_URL + 'Documenti/Privacy_Policy.htm','_system', 'location=yes');
  }

  getTermini()
  {
    this.myUtils.goToUrl(Constants.IMAGE_BASE_URL + 'Documenti/Termini.htm');
    // window.open(Constants.IMAGE_BASE_URL + 'Documenti/Termini.htm','_system', 'location=yes');
  }
}
