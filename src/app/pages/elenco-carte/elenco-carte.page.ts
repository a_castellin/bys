import { Component, OnInit } from '@angular/core';
import { MyUtils } from '../../services/my-utils.service'

@Component({
  selector: 'app-elenco-carte',
  templateUrl: './elenco-carte.page.html',
  styleUrls: ['./elenco-carte.page.scss'],
})
export class ElencoCartePage implements OnInit {

  constructor(
    private myUtils:MyUtils) { }

  ngOnInit() {
  }

}
