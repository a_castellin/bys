import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ElencoCartePage } from './elenco-carte.page';

const routes: Routes = [
  {
    path: '',
    component: ElencoCartePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ElencoCartePageRoutingModule {}
