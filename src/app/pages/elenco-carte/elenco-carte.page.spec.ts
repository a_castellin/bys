import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ElencoCartePage } from './elenco-carte.page';

describe('ElencoCartePage', () => {
  let component: ElencoCartePage;
  let fixture: ComponentFixture<ElencoCartePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElencoCartePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ElencoCartePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
