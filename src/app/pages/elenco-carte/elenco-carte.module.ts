import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ElencoCartePageRoutingModule } from './elenco-carte-routing.module';

import { ElencoCartePage } from './elenco-carte.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ElencoCartePageRoutingModule
  ],
  declarations: [ElencoCartePage]
})
export class ElencoCartePageModule {}
