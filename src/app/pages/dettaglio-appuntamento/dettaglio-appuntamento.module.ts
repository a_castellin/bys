import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DettaglioAppuntamentoPageRoutingModule } from './dettaglio-appuntamento-routing.module';

import { DettaglioAppuntamentoPage } from './dettaglio-appuntamento.page';
import { SideMenuComponent } from '../../components/side-menu/side-menu.component'
import { ComponentsModule } from '../../components/components.module'

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DettaglioAppuntamentoPageRoutingModule,
    ComponentsModule
  ],
  declarations: [DettaglioAppuntamentoPage]
})
export class DettaglioAppuntamentoPageModule {}
