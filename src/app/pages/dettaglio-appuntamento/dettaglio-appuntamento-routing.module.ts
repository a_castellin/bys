import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DettaglioAppuntamentoPage } from './dettaglio-appuntamento.page';

const routes: Routes = [
  {
    path: '',
    component: DettaglioAppuntamentoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DettaglioAppuntamentoPageRoutingModule {}
