import { Component, OnInit } from '@angular/core';
import { SvcRequestsService } from '../../services/svc-requests.service';
import { from } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import * as Constants from '../../constants'
import { MenuController } from '@ionic/angular';
import { Platform } from '@ionic/angular';
import { MyUtils } from '../../services/my-utils.service'

@Component({
  selector: 'app-Dettaglio-appuntamento',
  templateUrl: './Dettaglio-appuntamento.page.html',
  styleUrls: ['./Dettaglio-appuntamento.page.scss'],
})
export class DettaglioAppuntamentoPage implements OnInit {
  userInfo: any;
  marchioInfo: any;
  negozioInfo: any;
  appuntamentoInfo:any;
  note:string = '';
  backPage:string = 'elenco-appuntamenti';
  tipoAppuntamento:any;
  arTipiAppuntamento:any[]=[];
  compareWith:any;
  bEnableGestioneArticoli:Boolean = false;
  constructor(
    private platform: Platform,
    private router:Router, 
    private activatedRoute:ActivatedRoute, 
    private SvcService:SvcRequestsService, 
    private alertController:AlertController,
    private storage:Storage,
    private menu:MenuController,
    private myUtils:MyUtils
  ) { }

  Months: string[]=["Gen","Feb","Mar","Apr","Mag","Giu","Lug","Ago","Set","Ott","Nov","Dec"];
  Days: string[]=["Dom","Lun","Mar","Mer","Gio","Ven","Sab"];

  ngOnInit() {
    this.compareWith = this.compareWithFn;
    this.activatedRoute.queryParams.subscribe(
      params => {
        let requestData = JSON.parse(params['data']);
        this.userInfo = requestData.user;
        this.marchioInfo = requestData.marchio;
        this.negozioInfo = requestData.negozio;
        this.appuntamentoInfo = requestData.appuntamento;
        if(this.appuntamentoInfo != undefined && this.appuntamentoInfo['NOTA'] != undefined)
        {
          this.note = this.appuntamentoInfo['NOTA']
        }
        this.tipoAppuntamento = requestData.tipologiaAppuntamento;
        if(this.tipoAppuntamento == undefined || this.tipoAppuntamento == null)
        {
          this.tipoAppuntamento = {};
        }

        this.updateTipiAppuntamento();
      }
    );
  }

  getDataString(date:string)
  {
    let CurrentDate = new Date(date);
    return this.Days[CurrentDate.getDay()] + " " + CurrentDate.getDate() + " " + this.Months[CurrentDate.getMonth()] + " " + CurrentDate.getFullYear();
  }

  update()
  {
    this.appuntamentoInfo['NOTA'] = this.note;
    if(this.tipoAppuntamento != undefined && this.tipoAppuntamento['CODICE'] != undefined)
    {
      this.updateAppuntamento(this.appuntamentoInfo)
    }
    else
    {
      this.alertTipoAppuntamento();
    }
  }

  clear()
  {
    this.deleteAppuntamento(this.appuntamentoInfo)
  }

  back()
  {
    this.router.navigate(["/",this.backPage],
    {
      replaceUrl:true, 
      queryParams:{
        data:JSON.stringify(
        {
          "user":this.userInfo,
          "marchio":this.marchioInfo,
          "negozio":this.negozioInfo,
          "data": this.appuntamentoInfo['DATA_APPUNTAMENTO_STR'].toString().substr(0,8)
        })
      }
    });
  }

  getImageUrl(item)
  {
    // return Constants.IMAGE_BASE_URL + item['IMG'];
    return item['IMG'];
  }

  updateTipiAppuntamento(){
    this.arTipiAppuntamento = [];

    this.SvcService.getTipiAppuntamento(this.userInfo['IDUTENTE'],this.marchioInfo['CODICE'],this.negozioInfo['IDNEGOZIO']).subscribe(data=>{
      var success = (data["SUCCESS"]!=undefined?new Boolean(data["SUCCESS"]):false);
        if(success==true)
        {
          // console.dir(data["DATA"]);
          this.arTipiAppuntamento = data["DATA"];
          if(this.appuntamentoInfo != undefined && this.appuntamentoInfo['TIPOAP_CODICE'] > 0)
          {
            this.tipoAppuntamento = this.arTipiAppuntamento.find((item) => { 
              return item['CODICE'].toString() == this.appuntamentoInfo['TIPOAP_CODICE'].toString();}
              );
            this.EnableGestioneArticoli();
          }
        }
    })
  }

  EnableGestioneArticoli()
  {
    if(this.appuntamentoInfo != undefined)
    {
      if(this.tipoAppuntamento['CODICE'].toString() == '4' || this.tipoAppuntamento['CODICE'].toString() == '5')
      {
        if(this.appuntamentoInfo['TIPOAP_CODICE'].toString() == this.tipoAppuntamento['CODICE'].toString())
        {
          this.bEnableGestioneArticoli = true;
        }
        else
        {
          this.bEnableGestioneArticoli = false;
        }
      }
      else
      {
        this.bEnableGestioneArticoli = false;
      }
    }
    else
      this.bEnableGestioneArticoli = false;

  }

  compareWithFn(o1, o2) {
    return o1 === o2;
  };
  

  async deleteAppuntamento(item:any) {
    const alert = await this.alertController.create({
      header: '',
      message: "Confermi di voler eliminare l'appuntamento selezionato?",
      buttons: [{
        text: 'Ok',
        handler: () => {
          let dtData = new Date(item['DATA_STR']);
          let strDataOra = dtData.getFullYear()+""+(dtData.getMonth() + 1).toString().padStart(2,"0")+""+dtData.getDate().toString().padStart(2,"0") + "/" + item['ORA_STR'];
          this.SvcService.deleteAppuntamento(this.userInfo['IDUTENTE'],this.marchioInfo['CODICE'],this.negozioInfo['IDNEGOZIO'],strDataOra).subscribe(data=>{
            var success = (data["SUCCESS"]!=undefined?new Boolean(data["SUCCESS"]):false);
              if(success==true)
              {
                //this.deleteAppuntamentoOK();
                this.back();
              }
              else
              {
                this.deleteAppuntamentoKO();
              }
          },
          error=>{
            this.deleteAppuntamentoKO();
          });
        }        
      },
      {
        text: 'Cancel'
      }],
      mode:"ios"
    });

    await alert.present();
  }

  async deleteAppuntamentoOK() {
    const alert = await this.alertController.create({
      header: '',
      message: "Cancellazione avvenuta con sucesso",
      buttons: ['Ok'],
      mode:"ios"
    });

    await alert.present();
  }

  async deleteAppuntamentoKO() {
    const alert = await this.alertController.create({
      header: 'Attenzione',
      message: "Si sono verificati dei problemi durante la cancellazione!",
      buttons: ['Ok'],
      mode:"ios"
    });

    await alert.present();
  }

  async updateAppuntamento(item:any) {
    const alert = await this.alertController.create({
      header: '',
      message: "Confermi di voler procedere con la richiesta di appuntamento?",
      buttons: [{
        text: 'Ok',
        handler: () => {
          let dtData = new Date(item['DATA_STR']);
          let strDataOra = dtData.getFullYear()+""+(dtData.getMonth() + 1).toString().padStart(2,"0")+""+dtData.getDate().toString().padStart(2,"0") + "/" + item['ORA_STR'];
          this.SvcService.updateAppuntamento(this.userInfo['IDUTENTE'],this.marchioInfo['CODICE'],this.negozioInfo['IDNEGOZIO'],strDataOra,this.appuntamentoInfo['NOTA'],this.tipoAppuntamento['CODICE']).subscribe(data=>{
            var success = (data["SUCCESS"]!=undefined?new Boolean(data["SUCCESS"]):false);
              if(success==true)
              {
                this.appuntamentoInfo['TIPOAP_CODICE'] = this.tipoAppuntamento['CODICE'];
                if(this.appuntamentoInfo['TIPOAP_CODICE'].toString() == '4' || this.appuntamentoInfo['TIPOAP_CODICE'].toString() == '5')
                  this.updateAppuntamentoArticoli();
                else
                  this.back();
              }
              else
              {
                this.updateAppuntamentoKO();
              }
          },
          error=>{
            this.updateAppuntamentoKO();
          });
        }        
      },
      {
        text: 'Cancel'
      }],
      mode:"ios"
    });

    await alert.present();
  }

  async updateAppuntamentoArticoli() {
    const alert = await this.alertController.create({
      header: 'Selezione Articoli',
      message: "Caricamento avvenuta con sucesso.<br><br>Procedere con la selezione degli articoli?",
      buttons: [
        {
          text:'Sì',
          handler:()=>{
            this.gestisciArticoli();
          }
        },
        {
          text:'No',
          handler:()=>{this.back()}
        }],
        mode:"ios"
    });

    await alert.present();
  }

  async updateAppuntamentoKO() {
    const alert = await this.alertController.create({
      header: 'Attenzione',
      message: "Si sono verificati dei problemi durante l'invio della richiesta'!",
      buttons: ['Ok'],
      mode:"ios"
    });

    await alert.present();
  }

  async alertTipoAppuntamento() {
    const alert = await this.alertController.create({
      header: 'Attenzione',
      message: "Seleziomare la tipologia di appuntamento",
      buttons: ['Ok'],
      mode:"ios"
    });

    await alert.present();
  }
  
  openSideMenu(){
    this.menu.open('side')
  }

  gestisciArticoli()
  {
    this.router.navigate(["/","elenco-articoli"],
    {
      replaceUrl:true, 
      queryParams:{
        data:JSON.stringify(
        {
          "user":this.userInfo,
          "marchio":this.marchioInfo,
          "negozio":this.negozioInfo,
          "appuntamento":this.appuntamentoInfo,
          "fromPage":"elenco-appuntamenti",
          "auto":true
        })
      }
    });
  }

  getTestoGestioneArticoli()
  {
    switch(this.tipoAppuntamento['CODICE'].toString())
    {
      case '4':
        {
          return "Seleziona articoli da rendere";
        }
      case '5':
        {
          return "Seleziona articoli da sostituire";
        }
    }
  }

}
