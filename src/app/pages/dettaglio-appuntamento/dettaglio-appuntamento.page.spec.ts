import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DettaglioAppuntamentoPage } from './dettaglio-appuntamento.page';

describe('DettaglioAppuntamentoPage', () => {
  let component: DettaglioAppuntamentoPage;
  let fixture: ComponentFixture<DettaglioAppuntamentoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DettaglioAppuntamentoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DettaglioAppuntamentoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
