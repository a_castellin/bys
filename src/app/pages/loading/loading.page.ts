import { Component, OnInit} from '@angular/core';
import { SvcRequestsService } from '../../services/svc-requests.service';
import { from } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { MyUtils } from '../../services/my-utils.service'

@Component({
  selector: 'app-loading',
  templateUrl: './loading.page.html',
  styleUrls: ['./loading.page.scss'],
})
export class LoadingPage implements OnInit{
  nome:string;
  numero:string;
  username:string;
  password:string;
  constructor(
    private router:Router, 
    private activatedRoute:ActivatedRoute, 
    private SvcService:SvcRequestsService, 
    private alertController:AlertController,
    private storage:Storage,
    private myUtils:MyUtils
    ) { }

  ngOnInit() {      
    this.storage.get("nome").then((val) => {
        this.nome = val;
        this.storage.get("numero").then((val) => {
          this.numero = val;
          this.storage.get("password").then((val) => {
            this.password = val;
            this.login(this.nome,this.numero,this.password);
          });
        });
      });   
  }

  login(nome:string, numero:string, password:string)
  {
    if(nome!='' && numero!='' && password!=''){
      this.SvcService.login(nome, numero, password).subscribe(data=>{
        var success = (data["SUCCESS"]!=undefined?new Boolean(data["SUCCESS"]):false);
        if(success==true)
        {
          let userInfo = data["DATA"][0];
          this.storage.set("nome",nome);
          this.storage.set("numero",numero);
          this.storage.set("password",password);
          this.SvcService.setFCMToken(userInfo['IDUTENTE']);

          if(userInfo['ABILITATO'] != undefined && (Number)(userInfo['ABILITATO']) > 0)
          {
            if(userInfo['FORCE_CHANGE']!= undefined && (Number)(userInfo['FORCE_CHANGE']) == 0)
            {
              this.router.navigate(['/','elenco-appuntamenti'],{replaceUrl:true, queryParams:{data:JSON.stringify({"user":userInfo})}});
            }
            else
            {
              this.router.navigate(['/','change-password'],{replaceUrl:true, queryParams:{data:JSON.stringify({"user":userInfo, "back":"login"})}});
            }
          }
          else
          {
            this.router.navigate(['/','validate-user'],{replaceUrl:true, queryParams:{data:JSON.stringify({"user":userInfo})}});
          }       
        }
        else
        {
          this.router.navigate(['/','login'],{replaceUrl:true});
        }
      }
      , error => {
        this.router.navigate(['/','login'],{replaceUrl:true});
      });
    }
    else
    {     
      this.router.navigate(['/','login'],{replaceUrl:true});
    }
  }

}
