import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SelezioneOraPageRoutingModule } from './selezione-ora-routing.module';

import { SelezioneOraPage } from './selezione-ora.page';
import { SideMenuComponent } from '../../components/side-menu/side-menu.component'
import { ComponentsModule } from '../../components/components.module'

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SelezioneOraPageRoutingModule,
    ComponentsModule
  ],
  declarations: [SelezioneOraPage]
})
export class SelezioneOraPageModule {}
