import { Component, OnInit, AfterViewInit } from '@angular/core';
import { SvcRequestsService } from '../../services/svc-requests.service'
import { from } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { MenuController } from '@ionic/angular';
import { MyUtils } from '../../services/my-utils.service'

@Component({
  selector: 'app-selezione-ora',
  templateUrl: './selezione-ora.page.html',
  styleUrls: ['./selezione-ora.page.scss'],
})
export class SelezioneOraPage implements OnInit, AfterViewInit {
  arOre: any[]=[];
  userInfo: any;
  marchioInfo: any;
  negozioInfo: any;
  dateInfo:string;
  tipologiaAppuntamento:any;
  constructor(private router: Router, private activatedRoute: ActivatedRoute, private SvcService: SvcRequestsService, private menu:MenuController,
    private myUtils:MyUtils) { }
  Months: string[]=["Gen","Feb","Mar","Apr","Mag","Giu","Lug","Ago","Set","Ott","Nov","Dec"];
  Days: string[]=["Dom","Lun","Mar","Mer","Gio","Ven","Sab"];

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(
      params => {
        let requestData = JSON.parse(params['data']);
        this.userInfo = requestData.user;
        this.marchioInfo = requestData.marchio;
        this.negozioInfo = requestData.negozio;
        this.dateInfo = requestData.data;
        this.tipologiaAppuntamento = requestData.tipologiaAppuntamento;
        this.updateOreList();
      }
    );
  }

  ngAfterViewInit(){
    this.activatedRoute.queryParams.subscribe(
      params => {
         let requestData = JSON.parse(params['data']);
         this.userInfo = requestData.user;
         this.marchioInfo = requestData.marchio;
         this.negozioInfo = requestData.negozio;
         this.dateInfo = requestData.data;
         this.tipologiaAppuntamento = requestData.tipologiaAppuntamento;

         this.updateOreList();
      }
    );
  }

  getDataString(date:string)
  {
    let CurrentDate = new Date(date);
    return this.Days[CurrentDate.getDay()] + " " + CurrentDate.getDate() + " " + this.Months[CurrentDate.getMonth()] + " " + CurrentDate.getFullYear();
  }

  updateOreList()
  {
    this.SvcService.getAppuntamenti(this.userInfo['IDUTENTE'],this.marchioInfo['CODICE'],this.negozioInfo['IDNEGOZIO'], this.dateInfo, true).subscribe(data=>{
      var success = (data["SUCCESS"]!=undefined?new Boolean(data["SUCCESS"]):false);
        if(success==true)
        {
          let Info = data["DATA"][0];
          this.arOre = Info['ROWS'];

          let dtData = new Date();
          let strNow = dtData.getFullYear()+""+(dtData.getMonth() + 1).toString().padStart(2,"0")+""+dtData.getDate().toString().padStart(2,"0") + "/" + dtData.getHours().toString().padStart(2,"0")+":"+dtData.getMinutes().toString().padStart(2,"0");
          //DA VALUTARE SE NECESSARIO RIFORMATTARE DATA YYYYMMDD/HH:mm
          for(let i=0 ; i<this.arOre.length; i++)
          {
            let item = this.arOre[i];
            let strData:string = item['DATA_APPUNTAMENTO_STR'];
            item['DATA_STR'] = strData.substr(0,4) + '-' + strData.substr(4,2) + '-' + strData.substr(6,2);
            item['ORA_STR'] = strData.substr(9,2) + ':' + strData.substr(12,2);
            item['PASSED'] = item['DATA_APPUNTAMENTO_STR'] <= strNow;
            item['STATUS'] = (item['TIPOAP_CODICE'] != 0)?1:((item['NUMERO_POSTI'] <= 0)?2:0);
          }
          this.arOre = this.arOre.sort((a,b)=>{
            if(a['DATA_APPUNTAMENTO_STR'] < b['DATA_APPUNTAMENTO_STR'])
              return -1;
            if(a['DATA_APPUNTAMENTO_STR'] > b['DATA_APPUNTAMENTO_STR'])
              return 1;
            return 0;
          });

        }
    })
  }

  selectOra(item)
  {
    if(!item['PASSED'] && item['STATUS']!=2){
      this.router.navigate(["/","dettaglio-appuntamento"],
      {
        replaceUrl:true, 
        queryParams:{
          data:JSON.stringify(
          {
            "user":this.userInfo,
            "marchio":this.marchioInfo,
            "negozio":this.negozioInfo,
            "appuntamento":item,
            "tipologiaAppuntamento":this.tipologiaAppuntamento,
            "fromPage":"elenco-appuntamenti"
          })
        }
      });
    }
  }

  back()
  {
    this.router.navigate(["/",'selezione-giorno'],
    {
      replaceUrl:true, 
      queryParams:{
        data:JSON.stringify(
        {
          "user":this.userInfo,
          "marchio":this.marchioInfo,
          "negozio":this.negozioInfo,
          "tipologiaAppuntamento":this.tipologiaAppuntamento
        })
      }
    });
  }
  
  openSideMenu(){
    this.menu.open('side')
  }

}
