import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SelezioneOraPage } from './selezione-ora.page';

const routes: Routes = [
  {
    path: '',
    component: SelezioneOraPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SelezioneOraPageRoutingModule {}
