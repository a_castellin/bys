import { Component, OnInit, ErrorHandler} from '@angular/core';
import { SvcRequestsService } from '../../services/svc-requests.service';
import { from } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import * as Constants from '../../constants'
import { MenuController } from '@ionic/angular';
import { MyUtils } from '../../services/my-utils.service'
import {DomSanitizer} from '@angular/platform-browser'

@Component({
  selector: 'app-notifiche',
  templateUrl: './notifiche.page.html',
  styleUrls: ['./notifiche.page.scss'],
})
export class NotifichePage implements OnInit {
  arNotifiche: any[]=[];
  userInfo: any;
  marchioInfo: any;
  negozioInfo: any;
  city: string;
  constructor(
    private router: Router, 
    private activatedRoute: ActivatedRoute, 
    private SvcService: SvcRequestsService, 
    private alertController: AlertController,
    private menu:MenuController,
    private myUtils:MyUtils,
    private domSanitizer:DomSanitizer) { }

  Months: string[]=["Gen","Feb","Mar","Apr","Mag","Giu","Lug","Ago","Set","Ott","Nov","Dec"];
  Days: string[]=["Dom","Lun","Mar","Mer","Gio","Ven","Sab"];
  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(
      params => {
        let requestData = JSON.parse(params['data']);
        this.userInfo = requestData.user;
        this.marchioInfo = requestData.marchio;
        this.negozioInfo = requestData.negozio;
        this.updateNotificheList();

      }
    );
  }


  getDataString(date:string)
  {
    let CurrentDate = new Date(date);
    return this.Days[CurrentDate.getDay()] + " " + CurrentDate.getDate() + " " + this.Months[CurrentDate.getMonth()] + " " + CurrentDate.getFullYear();
  }

  getElencoDate()
  {
    let arDate = [];
    let map = new Map()
    for(let item of this.arNotifiche)
    {
      if(!map.has(item['DATA_STR']))
      {
        map.set(item['DATA_STR'],true);
        arDate.push(item['DATA_STR']);
      }
    }

    // console.dir(arDate);

    arDate = arDate.sort((a,b)=>{
      if(a > b)
        return -1;
      else if(a < b)
        return 1;
      else
        return 0
    });

    // console.dir(arDate);

    return arDate;
  }

  getNotifiche(date:string)
  {
    return this.arNotifiche.filter(item=>{return item['DATA_STR'] == date}).sort((a,b)=>{
      if(a['ORA_STR'] > b['ORA_STR'])
        return 1;
      if(a['ORA_STR'] < b['ORA_STR'])
        return -1;
      return 0;
      
    });
  }

  updateNotificheList(){
    this.SvcService.getNotifiche(this.userInfo['IDUTENTE']).subscribe(data=>{
      var success = (data["SUCCESS"]!=undefined?new Boolean(data["SUCCESS"]):false);
        if(success==true)
        {
          this.arNotifiche = data["DATA"];
          
          for(let i=0 ; i<this.arNotifiche.length; i++)
          {
            let item = this.arNotifiche[i];
            let strData:string = item['DATA_NOTIFICA_STR'];
            item['DATA_STR'] = strData.substr(0,4) + '-' + strData.substr(4,2) + '-' + strData.substr(6,2);
            item['ORA_STR'] = strData.substr(9,2) + ':' + strData.substr(12,2);
          }

          this.arNotifiche = this.arNotifiche.sort((a,b)=>{
            if(a['DATA_NOTIFICA_STR'] < b['DATA_NOTIFICA_STR'])
              return -1;
            if(a['DATA_NOTIFICA_STR'] > b['DATA_NOTIFICA_STR'])
              return 1;
            return 0;
          });

        }
    })
  }

  showNotifica(item)
  {
    this.notificaPush(item)
  }

  logOut()
  {
    this.router.navigate(["/","login"],{replaceUrl:true});
  }

  getIcon(item)
  {
    if(item['AZIENDA_INFO']!=undefined)
    {
      if(item['AZIENDA_INFO']['ICON']!=undefined && item['AZIENDA_INFO']['ICON']!= '')
        // return Constants.IMAGE_BASE_URL + item['AZIENDA_INFO']['ICON'];
        return item['AZIENDA_INFO']['ICON'];
      
      if(item['AZIENDA_INFO']['IMG']!=undefined && item['AZIENDA_INFO']['IMG']!= '')
        // return Constants.IMAGE_BASE_URL + item['AZIENDA_INFO']['IMG'];
        return item['AZIENDA_INFO']['IMG'];
    }

    return ''
  }

  checkIcon(item)
  {
    if(item['AZIENDA_INFO']!=undefined)
    {
      if(item['AZIENDA_INFO']['ICON']!=undefined && item['AZIENDA_INFO']['ICON']!= '')
        return true;
      
      if(item['AZIENDA_INFO']['IMG']!=undefined && item['AZIENDA_INFO']['IMG']!= '')
        return true;
    }

    return false;
  }

  getRagSoc(item)
  {
    if(item['NEGOZIO_INFO']!=undefined && item['NEGOZIO_INFO']['RAGIONE_SOCIALE'] != undefined)
      return item['NEGOZIO_INFO']['RAGIONE_SOCIALE'];
    return ''
  }
  
  openSideMenu(){
    this.menu.open('side')
  }

  updateNotifica(item)
  {
    this.SvcService.updateNotifiche(item).subscribe(data=>{
      var success = (data["SUCCESS"]!=undefined?new Boolean(data["SUCCESS"]):false);
        if(success==true)
        {
          item['STATUS'] = 1;
        }
    });
  }
  
  async notificaPush(item:any) {
    if(item['URL_NOTIFICA']!=undefined && item['URL_NOTIFICA']!='')
    {
      const alert = await this.alertController.create({
        cssClass: 'PushNotificationAlert',
        header: 'Notifica!',
        subHeader: item['TITOLO'],
        message: item['MESSAGGIO_EXT'],
        buttons: [
        {
          text:'Dettagli',
          //cssClass:'alert-link-button',
          handler:()=>{
            this.updateNotifica(item);
            this.myUtils.goToUrl(item['URL_NOTIFICA']);
          }
        },
        {
          text:'Letto',
          handler:()=>{
            this.updateNotifica(item);
          }
        },
        {
          text:'Chiudi',
          handler:()=>{}
        }],
        mode:"ios"
      });

      await alert.present();
    }
    else
    {
      const alert = await this.alertController.create({
        cssClass: 'PushNotificationAlert',
        header: 'Notifica!',
        subHeader: item['TITOLO'],
        message: item['MESSAGGIO_EXT'],
        buttons: [{
          text:'Letto',
          handler:()=>{
            this.updateNotifica(item);
          }
        },
        {
          text:'Chiudi',
          handler:()=>{}
        }],
        mode:"ios"
      });

      await alert.present();
    }
  }

}
