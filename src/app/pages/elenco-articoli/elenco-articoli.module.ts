import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ElencoArticoliPageRoutingModule } from './elenco-articoli-routing.module';

import { ElencoArticoliPage } from './elenco-articoli.page';
import { ComponentsModule } from '../../components/components.module'
import { ImageModPageModule } from '../../modal/image-mod/image-mod.module'

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ElencoArticoliPageRoutingModule,
    ComponentsModule,
    ImageModPageModule
  ],
  declarations: [ElencoArticoliPage]
})
export class ElencoArticoliPageModule {}
