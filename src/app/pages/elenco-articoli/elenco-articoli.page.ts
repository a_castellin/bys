import { Component, OnInit} from '@angular/core';
import { SvcRequestsService } from '../../services/svc-requests.service';
import { from } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import * as Constants from '../../constants'
import { MenuController } from '@ionic/angular';
import { ModalController } from '@ionic/angular';
import { ImageModPage } from '../../modal/image-mod/image-mod.page'
import { MyUtils } from '../../services/my-utils.service'

// Typescript custom enum for search types (optional)
export enum DataFormatType {
  DDDMMMYYYY = 0,
  YYYYMMDD = 1
}

@Component({
  selector: 'app-elenco-articoli',
  templateUrl: './elenco-articoli.page.html',
  styleUrls: ['./elenco-articoli.page.scss'],
})
export class ElencoArticoliPage implements OnInit{
  userInfo: any;
  marchioInfo: any;
  negozioInfo: any;
  appuntamentoInfo:any;
  arArticoli: any[]=[];
  autoSelect: boolean = false;

  constructor(
    private router: Router, 
    private activatedRoute: ActivatedRoute, 
    private SvcService: SvcRequestsService, 
    private alertController: AlertController,
    private modalController:ModalController,
    private menu:MenuController,
    private myUtils:MyUtils) { }

  Months: string[]=["Gen","Feb","Mar","Apr","Mag","Giu","Lug","Ago","Set","Ott","Nov","Dec"];
  Days: string[]=["Dom","Lun","Mar","Mer","Gio","Ven","Sab"];
  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(
      params => {
        let requestData = JSON.parse(params['data']);
        this.userInfo = requestData.user;
        this.marchioInfo = requestData.marchio;
        this.negozioInfo = requestData.negozio;
        this.appuntamentoInfo = requestData.appuntamento;
        let auto:boolean = requestData.auto;
        if(auto != undefined)
          this.autoSelect = auto;
        
        this.updateArticoliList();
      }
    );
  }

  getDataString(date:string)
  {
    let CurrentDate = new Date(date);
    return this.Days[CurrentDate.getDay()] + " " + CurrentDate.getDate() + " " + this.Months[CurrentDate.getMonth()] + " " + CurrentDate.getFullYear();
  }

  getElencoScontrini()
  {
    let arDate = [];
    let map = new Map()
    for(let item of this.arArticoli)
    {
      if(!map.has(item['BARCODE_SCONTRINO']))
      {
        map.set(item['BARCODE_SCONTRINO'],true);
        arDate.push(item['BARCODE_SCONTRINO']);
      }
    }
    return arDate;
  }

  getArticoli(barcode:string)
  {
    return this.arArticoli.filter(item=>{return item['BARCODE_SCONTRINO'] == barcode})/*.sort((a,b)=>{
      if(a['ORA_STR'] > b['ORA_STR'])
        return 1;
      if(a['ORA_STR'] < b['ORA_STR'])
        return -1;
      return 0;
      
    });*/
  }

  nuovoArticolo()
  {
    this.router.navigate(["/","elenco-scontrini"],
    {
      replaceUrl:true, 
      queryParams:{
        data:JSON.stringify(
        {
          "user":this.userInfo,
          "marchio":this.marchioInfo,
          "negozio":this.negozioInfo,
          "appuntamento": this.appuntamentoInfo
        })
      }
    });
  }

  updateArticoliList(){
    let dtData = new Date(this.appuntamentoInfo['DATA_STR']);
    let strDataOra = dtData.getFullYear()+""+(dtData.getMonth() + 1).toString().padStart(2,"0")+""+dtData.getDate().toString().padStart(2,"0") + "/" + this.appuntamentoInfo['ORA_STR'];
     
    this.SvcService.getArticoliSelezionati(this.userInfo['IDUTENTE'],this.marchioInfo['CODICE'],this.negozioInfo['IDNEGOZIO'],strDataOra).subscribe(data=>{
      var success = (data["SUCCESS"]!=undefined?new Boolean(data["SUCCESS"]):false);
        if(success==true)
        {
          this.arArticoli = data["DATA"];
          if(this.arArticoli != undefined && this.arArticoli.length == 0 && this.autoSelect)
          {
            setTimeout(()=>{this.nuovoArticolo()},500);
          }
        }
    })
  }

  back()
  {
    this.router.navigate(["/","elenco-appuntamenti"],
    {
      replaceUrl:true, 
      queryParams:{
        data:JSON.stringify(
        {
          "user":this.userInfo
        })
      }
    });
  }

  getIcon(item)
  {
    if(item['AZIENDA_INFO']!=undefined)
    {
      if(item['AZIENDA_INFO']['ICON']!=undefined && item['AZIENDA_INFO']['ICON']!= '')
        // return Constants.IMAGE_BASE_URL + item['AZIENDA_INFO']['ICON'];
        return item['AZIENDA_INFO']['ICON'];
      
      if(item['AZIENDA_INFO']['IMG']!=undefined && item['AZIENDA_INFO']['IMG']!= '')
        // return Constants.IMAGE_BASE_URL + item['AZIENDA_INFO']['IMG'];
        return item['AZIENDA_INFO']['IMG'];
    }

    return ''
  }

  checkIcon(item)
  {
    if(item['AZIENDA_INFO']!=undefined)
    {
      if(item['AZIENDA_INFO']['ICON']!=undefined && item['AZIENDA_INFO']['ICON']!= '')
        return true;
      
      if(item['AZIENDA_INFO']['IMG']!=undefined && item['AZIENDA_INFO']['IMG']!= '')
        return true;
    }

    return false;
  }
  
  openSideMenu(){
    this.menu.open('side')
  }

  removeItem(item:any)
  {
    this.deleteArticolo(item);
  }

  getImageUrl(item)
  {
    return item['ARTICOLO_IMAGE'];
  }

  async deleteArticolo(item:any) {
    const alert = await this.alertController.create({
      header: 'Attenzione!',
      message: "Per modificare un articolo già inserito è necessario rimuoverlo e procedere nuovamente al suo inserimento.<br>Procedere con l'operazione?",
      buttons: [{
        text: 'Sì',
        handler: () => {
          let dtData = new Date(this.appuntamentoInfo['DATA_STR']);
          let strDataOra = dtData.getFullYear()+""+(dtData.getMonth() + 1).toString().padStart(2,"0")+""+dtData.getDate().toString().padStart(2,"0") + "/" + this.appuntamentoInfo['ORA_STR'];
     
          this.SvcService.removeCambioReso(this.userInfo['IDUTENTE'],this.marchioInfo['CODICE'],this.negozioInfo['IDNEGOZIO'],strDataOra,item).subscribe(data=>{
            var success = (data["SUCCESS"]!=undefined?new Boolean(data["SUCCESS"]):false);
              if(success==true)
              {
                this.updateArticoliList();
              }
              else
              {
                this.deleteAppuntamentoKO();
              }
          },
          error=>{
            this.deleteAppuntamentoKO();
          });
        }        
      },
      {
        text: 'No'
      }],
      mode:"ios"
    });

    await alert.present();
  }

  async deleteAppuntamentoKO() {
    const alert = await this.alertController.create({
      header: 'Attenzione',
      message: "Si sono verificati dei problemi durante la cancellazione!",
      buttons: ['Ok'],
      mode:"ios"
    });

    await alert.present();
  }

  getTipologia()
  {
    switch(this.appuntamentoInfo['TIPOAP_CODICE'].toString())
    {
      case '4':
        {
          return "Cambio Merce";
        }
      case '5':
        {
          return "Cambio Taglia";
        }   
    }
  }

  async presentModal(item:any) {
    const modal = await this.modalController.create({
      component: ImageModPage,
      cssClass: 'modal-image-component',
      componentProps: {
        'imgAbsPath': this.getImageUrl(item)
      }
    });
    return await modal.present();
  }
}
