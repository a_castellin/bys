import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ElencoArticoliPage } from './elenco-articoli.page';

const routes: Routes = [
  {
    path: '',
    component: ElencoArticoliPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ElencoArticoliPageRoutingModule {}
