import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ElencoArticoliPage } from './elenco-articoli.page';

describe('ElencoArticoliPage', () => {
  let component: ElencoArticoliPage;
  let fixture: ComponentFixture<ElencoArticoliPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElencoArticoliPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ElencoArticoliPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
