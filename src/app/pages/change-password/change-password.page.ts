
import { Component, OnInit, AfterViewInit } from '@angular/core';
import { SvcRequestsService } from '../../services/svc-requests.service';
import { from } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { MyUtils } from '../../services/my-utils.service'

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.page.html',
  styleUrls: ['./change-password.page.scss'],
})
export class ChangePasswordPage implements OnInit, AfterViewInit  { 
  old_password: string;
  new_password: string;
  conferma_password: string;
  userInfo: any;
  backPage: string = 'login';

  constructor(
    private router: Router, 
    private activatedRoute: ActivatedRoute, 
    private SvcService: SvcRequestsService, 
    private alertController:AlertController,
    private storage:Storage,
    private myUtils:MyUtils) { }


  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(
      params => {
        let requestData = JSON.parse(params['data']);
        this.userInfo = requestData.user;
        let sBack:string = requestData.back;
        if(sBack != undefined && sBack != '')
        {
          this.backPage = sBack;
        }
      }
    );
  }

  ngAfterViewInit(){
  }

  cancel(){
    this.back();
  }

  resetHighlightClass()
  {
    let elems = document.getElementsByTagName('ion-item');
    for(let i=0; i<elems.length; i++)
    {
      elems[i].classList.remove('highlight');
    }
  }

  changePassword(){
    let bOk = true;
    this.resetHighlightClass();

    if(this.old_password == '')
    {
      bOk = false;
      let elem = document.getElementById('oldPassword');
        elem.classList.add('highlight');
    }

    if(this.new_password == '')
    {
      bOk = false;
      let elem = document.getElementById('newPassword');
        elem.classList.add('highlight');
    }

    if(this.conferma_password == '')
    {
      bOk = false;
      let elem = document.getElementById('confermaPassword');
        elem.classList.add('highlight');
    }

    if(bOk)
    {
      if(this.new_password != this.conferma_password)
      {
        let elem = document.getElementById('confermaPassword');
        elem.classList.add('highlight');
      }
      else
      {
        this.SvcService.passwordChange(this.userInfo['IDUTENTE'], this.old_password, this.new_password).subscribe(data=>{
          var success = (data["SUCCESS"]!=undefined?new Boolean(data["SUCCESS"]):false);
            if(success==true)
            {          
              this.changePasswordCompleteAlert();
            }
            else
            {
              this.DefualtAlert("Attenzione!",data["ERRORMESSAGE"]);
            }
        });
      }
    }
  }

  async DefualtAlert(header:string, message:string) {
    const alert = await this.alertController.create({
      header: header,
      message: message,
      buttons: ['OK'],
      mode:"ios"
    });

    await alert.present();
  }

  async changePasswordCompleteAlert() {
    const alert = await this.alertController.create({
      header: '',
      message: 'Cambio password avvenuto con successo',
      buttons: [{
        text: 'Ok',
        handler: () => {
          this.storage.set("password",this.new_password);
          this.router.navigate(["/","loading"],{replaceUrl:true});
        }
      }],
      mode:"ios"
    });

    await alert.present();
  }

  back()
  {
    this.router.navigate(["/",this.backPage],
    {
      replaceUrl:true, 
      queryParams:{
        data:JSON.stringify(
        {
          "user":this.userInfo
        })
      }
    });
  }

}
