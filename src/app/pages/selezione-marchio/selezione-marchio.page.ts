import { Component, OnInit, AfterViewInit } from '@angular/core';
import { SvcRequestsService } from '../../services/svc-requests.service';
import { from } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import * as Constants from '../../constants'
import { MenuController } from '@ionic/angular';
import { MyUtils } from '../../services/my-utils.service'


@Component({
  selector: 'app-selezione-marchio',
  templateUrl: './selezione-marchio.page.html',
  styleUrls: ['./selezione-marchio.page.scss'],
})
export class SelezioneMarchioPage implements OnInit, AfterViewInit {
  arMarchi: any[]=[];
  userInfo: any;
  autoSelect: boolean = false;

  constructor(private router: Router, private activatedRoute: ActivatedRoute, private SvcService: SvcRequestsService,private menu:MenuController,
    private myUtils:MyUtils) { }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(
      params => {
        let requestData = JSON.parse(params['data']);
        this.userInfo = requestData.user;
        let auto = requestData.autoSelect;
         if(auto != undefined)
         {
           this.autoSelect = auto;
         }
        this.updateMarchiList();
      }
    );
  }

  ngAfterViewInit(){
  }

  updateMarchiList()
  {
    this.SvcService.getMarchi(this.userInfo['IDUTENTE']).subscribe(data=>{
      var success = (data["SUCCESS"]!=undefined?new Boolean(data["SUCCESS"]):false);
        if(success==true)
        {
          this.arMarchi = data["DATA"];
          if(this.autoSelect && this.arMarchi.length == 1)
          {
            if(this.arMarchi[0]['CODICE'] != undefined)
            {
              setTimeout(()=>{
                this.router.navigate(
                  ["/","selezione-negozio"],
                  {
                    replaceUrl:true, 
                    queryParams:{
                      data:JSON.stringify(
                        {
                          "user":this.userInfo,
                          "marchio":this.arMarchi[0],
                          "autoSelect":true
                        })
                      }
                    });
              },350);
            }
          }
        }
        else
        {
          this.router.navigate(['/','login'],{replaceUrl:true});
        }
    })
  }

  selectNegozio(item:any)
  {
    if(item['CODICE'] != undefined)
    {
      this.router.navigate(["/","selezione-negozio"],{replaceUrl:true, queryParams:{data:JSON.stringify({"user":this.userInfo,"marchio":item})}});
    }
  }

  back()
  {
    this.router.navigate(["/",'elenco-appuntamenti'],
    {
      replaceUrl:true, 
      queryParams:{
        data:JSON.stringify(
        {
          "user":this.userInfo
        })
      }
    });
  }

  getImageUrl(item)
  {
    // return Constants.IMAGE_BASE_URL + item['IMG'];
    return item['IMG'];
  }
  
  openSideMenu(){
    this.menu.open('side')
  }

}
