import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SelezioneMarchioPageRoutingModule } from './selezione-marchio-routing.module';

import { SelezioneMarchioPage } from './selezione-marchio.page';
import { SideMenuComponent } from '../../components/side-menu/side-menu.component'
import { ComponentsModule } from '../../components/components.module'

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SelezioneMarchioPageRoutingModule,
    ComponentsModule
  ],
  declarations: [SelezioneMarchioPage]
})
export class SelezioneMarchioPageModule {}
