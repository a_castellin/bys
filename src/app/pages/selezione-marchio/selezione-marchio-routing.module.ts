import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SelezioneMarchioPage } from './selezione-marchio.page';

const routes: Routes = [
  {
    path: '',
    component: SelezioneMarchioPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SelezioneMarchioPageRoutingModule {}
