import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SelezioneTipologiaPage } from './selezione-tipologia.page';

describe('SelezioneTipologiaPage', () => {
  let component: SelezioneTipologiaPage;
  let fixture: ComponentFixture<SelezioneTipologiaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelezioneTipologiaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SelezioneTipologiaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
