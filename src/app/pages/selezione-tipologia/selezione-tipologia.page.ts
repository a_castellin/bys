import { Component, OnInit, AfterViewInit } from '@angular/core';
import { SvcRequestsService } from '../../services/svc-requests.service'
import { from } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { MenuController } from '@ionic/angular';
import * as Constants from '../../constants'
import { MyUtils } from '../../services/my-utils.service'


@Component({
  selector: 'app-selezione-tipologia',
  templateUrl: './selezione-tipologia.page.html',
  styleUrls: ['./selezione-tipologia.page.scss'],
})
export class SelezioneTipologiaPage implements OnInit, AfterViewInit {
  arTipiAppuntamento: any[]=[];
  userInfo: any;
  marchioInfo: any;
  negozioInfo: any;

  constructor(
    private router: Router, 
    private activatedRoute: ActivatedRoute, 
    private SvcService: SvcRequestsService, 
    private menu:MenuController,
    private myUtils:MyUtils) { }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(
      params => {
         let requestData = JSON.parse(params['data']);
         this.userInfo = requestData.user;
         this.marchioInfo = requestData.marchio
         this.negozioInfo = requestData.negozio

         this.updateTipiAppuntamento();
      }
    );
  }

  ngAfterViewInit(){
    
  }

  updateTipiAppuntamento(){
    this.arTipiAppuntamento = [];

    this.SvcService.getTipiAppuntamento(this.userInfo['IDUTENTE'],this.marchioInfo['CODICE'],this.negozioInfo['IDNEGOZIO']).subscribe(data=>{
      var success = (data["SUCCESS"]!=undefined?new Boolean(data["SUCCESS"]):false);
        if(success==true)
        {
          // console.dir(data["DATA"]);
          this.arTipiAppuntamento = data["DATA"];
        }
    })
  }

  selectTipoAppuntamento(item)
  {
    this.router.navigate(["/","selezione-giorno"],
    {
      replaceUrl:true, 
      queryParams:{
        data:JSON.stringify(
        {
          "user":this.userInfo,
          "marchio":this.marchioInfo,
          "negozio":this.negozioInfo,
          "tipologiaAppuntamento":item
        })
      }
    });
  }

  back()
  {
    this.router.navigate(["/",'selezione-negozio'],
    {
      replaceUrl:true, 
      queryParams:{
        data:JSON.stringify(
        {
          "user":this.userInfo,
          "marchio":this.marchioInfo
        })
      }
    });
  }
  
  openSideMenu(){
    this.menu.open('side')
  }

  getImageUrl(item:any)
  {
    return Constants.IMAGE_BASE_URL + item['ICON'];
  }

}
