import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SelezioneTipologiaPageRoutingModule } from './selezione-tipologia-routing.module';

import { SelezioneTipologiaPage } from './selezione-tipologia.page';
import { ComponentsModule } from '../../components/components.module'

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SelezioneTipologiaPageRoutingModule,
    ComponentsModule
  ],
  declarations: [SelezioneTipologiaPage]
})
export class SelezioneTipologiaPageModule {}
